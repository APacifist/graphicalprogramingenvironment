﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgramingEnvironment
{
    interface IHandler
    {
        IHandler SetNext(IHandler handler);
        object Handle(object request, Operation op, Variable var, Parser p, Methods method, string[] args, bool run, string[] fracturedLine, MainForm mainForm, string[] fracturedLineValues);
    }
}
