﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Contains the methods to check if a given line contains a declaration
    /// </summary>
    class DeclarationHandler : AbstractHandler
    {
        /// <summary>
        /// Overrides the 'Handle' method, checks to see if the given input is a variable assignment
        /// </summary>
        /// <param name="request">The line to be checked</param>
        /// <param name="op">Instance of the 'Operation' class</param>
        /// <param name="var">Instance of the 'Variable' class</param>
        /// <param name="p">Instance of the 'Parser' class</param>
        /// <param name="method">Instance of the 'Method' class</param>
        /// <param name="args">The arguments of the command</param>
        /// <param name="run">Whether or not the program is in syntax-checking mode</param>
        /// <param name="fracturedLine">The raw line input</param>
        /// <param name="mainForm">Instance of the 'MainForm' form</param>
        /// <param name="fracturedLineValues">The values of any variables contained in the input</param>
        /// <returns>Either returns an informative string, or the base handler method - used to 'handle' the next object</returns>
        public override object Handle(object request, Operation op, Variable var, Parser p, Methods method, string[] args, bool run, string[] fracturedLine, MainForm mainForm, string[] fracturedLineValues)
        {
            //The variable declaration regex will have a false-positive for while loops and if statements, so they are explicitly excluded below
            if ((op.Matches(request as string).Equals("Declaration") == true) & (op.Matches(request as string).Equals("While_Loop") == false) & (op.Matches(request as string).Equals("IF_Statement") == false))
            {
                if (args[1].Equals("=")) //If the second value in the command string is an '=' sign...
                {
                    Console.WriteLine("Assigning variable...I think");//Debugging purposes
                    var.Set(run, fracturedLine[0], args[2]);//Call the 'Set' method from the 'Variable' class, stores the variable
                    return "Declaration: Recognised";//Return an informative string
                }
                //If the second argument isn't an '=', then the format isn't valid and the next handler should be checked
                return base.Handle(request, op, var, p, method, args, run, fracturedLine, mainForm, fracturedLineValues);
            }
            else//If the format is invalid, then the next handler should be checked, the code below runs using the value of the next handler object
            {
                return base.Handle(request, op, var, p, method, args, run, fracturedLine, mainForm, fracturedLineValues);
            }
        }
    }
}
