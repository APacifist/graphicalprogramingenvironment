﻿using System;
using System.Configuration;
using System.Drawing;
using System.Linq.Expressions;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Handles manipulating and drawing a square, inherits properties from the 'Rectangle' class
    /// - By Joshua Hooper: 33532281
    /// </summary>
    class Square : Rectangle
    {
        public int size; //Creates the 'size' variable for use throughout this class
        /// <summary>
        /// Handles setting up a Square with a default size of 20, also is passed the 'testingMode' boolean
        /// </summary>
        /// <param name="testingMode">Whether or not testing mode is active</param>
        public Square(bool testingMode) : base(testingMode)
        {
            size = 20; //Assigns the value 20 to the 'size' variable
        }
        /// <summary>
        /// Sets up a Square with the various properties needed to draw it, along with the testingMode boolean for testing purposes
        /// </summary>
        /// <param name="pen">The pen that will be used to draw the square</param>
        /// <param name="x">The 'x' value of the square</param>
        /// <param name="y">The 'y' value of the square</param>
        /// <param name="size">The width/height of the square</param>
        /// <param name="testingMode">Whether or not testing mode is active</param>
        public Square(Pen pen, int x, int y, int size, bool testingMode) : base(pen, x, y, size, size, testingMode)
        {
            this.size = size; //Assigns the value of the parameter 'size' to the 'size' variable
        }
        /// <summary>
        /// Overrides the 'draw' method to add functionality to it
        /// Calls the 'draw' method from 'Rectangle' to graphically draw a square
        /// </summary>
        /// <param name="g">Graphics Context</param>
        public override void draw(Graphics g)
        {
            base.draw(g); //uses the base 'draw' method (from 'Rectangle') to draw a normal square
        }
        /// <summary>
        /// Overrides the 'drawFilled' method to add functionality to it
        /// Calls the 'draw' method from 'Rectangle' to graphically draw a filled square
        /// </summary>
        /// <param name="g">Graphics Context</param>
        public override void drawFilled(Graphics g)
        {
            base.drawFilled(g);//uses the base 'drawFilled' method (from 'Rectangle') to draw a filled square
        }
    }
}
