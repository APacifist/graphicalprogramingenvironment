﻿using System;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Handles the defining, storing, retrieving and executing of methods in the program
    /// </summary>
   public class Methods : Command
    {
        /// <summary>
        /// Contains the names, start and end lines of user-declared methods
        /// </summary>
        public string[,] methodArray = new string[5, 3]; /*Defining a 2D array, with three columns per row:
                                                          ([MethodName],[MethodStartLine],[MethodEndLine])*/

        int lastAddedX; //Keeping track of where, in the array, the last method was assingned to
        int foundAt; //Keeps track of where, in the array, the desired method was found

        /* public override void ExecuteLoop(MainForm main, Variable var, Parser p, int start, bool run)
         {
             throw new NotImplementedException();
         }*/

        /// <summary>
        /// Stores the name, start line, and end line inside the array, then makes sure the code inside the method isn't run until called
        /// </summary>
        /// <param name="main">The main form - used to search the text for a specific string</param>
        /// <param name="p">The 'Parser' class - used to set the variables needed to skip lines of code</param>
        /// <param name="args">Stores the input of the method's name, starting line and ending line</param>
        /// <param name="run">Whether the method is being called in syntax-checking mode</param>
        public void DefinineMethod(MainForm main,Parser p, bool run = true, params string[] args)
        {
            int endLine = FindStr(main, "enddef"); //Looks for "enddef" inside the textbox's text
            if(endLine != -1) //The method will only return '-1' if no string was found, if this is the case, the users must be informed of their mistake
            {
                if (run)
                {
                    Set(run, args[0], args[1], endLine.ToString()); //Calls the 'Set' method of this class to assign the given values to the appropriate area in the array
                                                                    //Set(Name, Method Definition Start Line, Method Definition End Line)
                    Console.WriteLine("Jumping to line: " + (endLine + 1) + " (Index: " + endLine + ")");//Debugging purposes only
                    p.skipLines = true; //This lets the 'Parser' class know that some user code will need to be skipped
                    p.skipTo = (endLine); //This tells the 'Parser' class what line to skip until
                }
            }
            else //Inform the user of their mistake by throwing an exception, to be handled within the 'Parser' class
            {
                throw new Exception("ERROR: No enclosing 'enddef' provided");
            }
            
        }

        /// <summary>
        /// Handles the 'calling' of a user-defined method
        /// </summary>
        /// <param name="main">The main form - used to search the text for a specific string</param>
        /// <param name="p">The 'Parser' class - used to set the variables needed to skip lines of code</param>
        /// <param name="args">Stores the input of the method's name, starting line and ending line</param>
        /// <param name="run">Whether the method is being called in syntax-checking mode</param>
        public void ExecuteMethod(MainForm main , Parser p, bool run = true, params string[] args)
        {
            Console.WriteLine("Method name is: " + args[0]); //Debugging purposes only
            Console.WriteLine("Stored method name is: " + methodArray[0, 0]);//Debugging purposes only
            if(run)
                {
                string tempVar = this.Get(args[0]); //Searches for the given method name, looking inside the methodArray
                String[] lines = main.activeWindow.Lines; //Takes the lines from the currently active textbox
                if (!(tempVar.Equals("false"))) //If 'tempVar' gives anything other than 'false', the methodname was valid, so the below code is executed
                {
                    string[] tempMethodArray = tempVar.Split(' '); //'tempVar' contains the start and end line of the method, seperated by whitespace, so split it
                    p.counter = int.Parse(tempMethodArray[0]);//Set the 'Parser' class'counter variable, needed for line skipping, and accurate error reporting
                    for (int i = (int.Parse(tempMethodArray[0])); i < int.Parse(tempMethodArray[1]); i++) //Whilst between the start and end lines of the method
                    {
                        Console.WriteLine("Current (New) Line: " + lines[i]); //Debugging purposes only
                        p.ParseCommand(lines[i].ToLower().Trim()); //Execute the 'ParseCommand' method from 'Parser' class on index 'i' of the array
                    }
                    Console.WriteLine("finished running method code");//Debugging purposes only
                }
                else //If the method couldn't be found, the name must be invalid, so an exception is thrown to inform the user of this
                {
                    throw new Exception("ERROR: No method found with the given name");//Caught inside the 'Parser' class
                }
            }
        }
        /// <summary>
        /// Overrides the 'Get' method to allow values to be retrieved from the 'methodArray' array
        /// </summary>
        /// <param name="args">The method name to be searched for</param>
        /// <returns></returns>
        public override string Get(params string[] args)
        {
            if (methodArray.Length != 0)
            { //Skip the whole search if the array is empty
                for (int k = 0; k < 5; k++) //Loop through the array looking at the first column(the variable name column)
                {
                    try
                    {
                        if (methodArray[k, 0] != null) //Don't bother trying to read it if it's null
                        {
                            Console.WriteLine("Analysing: " + methodArray[k, 0] + ", does it look like " + args[0] + "?");//Debugging purposes only
                            if (methodArray[k, 0].Equals(args[0])) //
                            {
                                Console.WriteLine("Yes!");
                                foundAt = k;
                               // if (!args[1].Equals(""))
                              //  {
                                    return (methodArray[k, 1] + " " + methodArray[k, 2]); //Returns the values for the start and end location of the method
                               // }
                            }
                            Console.WriteLine("Nope...");
                        }
                    }
                    //Just in case the method attempts to compare a null value, this catch should avoid the program crashing
                    catch (NullReferenceException)
                    {
                        Console.WriteLine("Null reference reached inside 'Get' of 'Method' class");
                    }
                }
            }
            return "false"; //If the loop finishes without breaking out at any point, the value isn't present, so 'false' is returned

        }
        /// <summary>
        /// Enables the setting of new methods
        /// </summary>
        /// <param name="run">Whether or not the method has been called in syntax-checking mode</param>
        /// <param name="list">The arguments to be set (method name, start location and end location</param>
        public override void Set(bool run = true, params string[] list)
        {
            if (run)
            {
                int tempX = 0; //Setting default x and y references for the array
                int tempY = 0;
                /*setting the above variables to be equal to the location values of the last added variable so the next
                 * added variable is placed into a new slot
                 */
                tempX += lastAddedX;
                if (this.Get(list[0]).Equals("false")) //First, check that an entry of the variable doesn't already exist
                {//If its unique, go through the process of adding it.
                    methodArray[tempX, tempY] = list[0];
                    methodArray[tempX, (tempY + 1)] = list[1];
                    methodArray[tempX, (tempY + 2)] = list[2];
                    Console.WriteLine("Values to insert: " + list[1] + ", " + list[1] + ", " + list[2]);
                    Console.WriteLine("New Values of array positions: " + "1: " + methodArray[tempX, tempY] + " 2: " + methodArray[tempX, (tempY + 1)] + " 3: " + methodArray[tempX, (tempY + 2)]);
                    if (lastAddedX < 5) //Since there are limited slots, if the last added variable is at that limit, it swings back around to zero
                    {
                        lastAddedX += 1;
                    }
                    else
                    {
                        lastAddedX = 0;
                    }
                }
                else //If an entry for the variable already exists, override it with the new value
                {
                    methodArray[foundAt, tempY] = list[0];
                    methodArray[foundAt, (tempY + 1)] = list[1];
                    methodArray[foundAt, (tempY + 2)] = list[2];
                }
            }
        }
    }
}
