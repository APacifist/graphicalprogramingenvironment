﻿using System.Windows.Forms;
using ZBobb;

namespace GraphicalProgramingEnvironment
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pb_Canvas = new System.Windows.Forms.PictureBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.lbl_activeWindow = new System.Windows.Forms.Label();
            this.bu_Execute = new System.Windows.Forms.Button();
            this.consoleBackground = new System.Windows.Forms.Panel();
            this.codeWindow = new ZBobb.AlphaBlendTextBox();
            this.bu_clearcode2 = new System.Windows.Forms.Button();
            this.bu_clearcode1 = new System.Windows.Forms.Button();
            this.redLightOffCmd = new System.Windows.Forms.PictureBox();
            this.redLightOnCmd = new System.Windows.Forms.PictureBox();
            this.lbl_PenColour = new System.Windows.Forms.Label();
            this.multiLineLable = new System.Windows.Forms.Label();
            this.lbl_multiLine = new System.Windows.Forms.Label();
            this.redLightOnScript = new System.Windows.Forms.PictureBox();
            this.redLightOffScript = new System.Windows.Forms.PictureBox();
            this.buttonTimer = new System.Windows.Forms.Timer(this.components);
            this.pb_buttonDown = new System.Windows.Forms.PictureBox();
            this.loadButton = new System.Windows.Forms.PictureBox();
            this.saveButton = new System.Windows.Forms.PictureBox();
            this.lbl_Red = new System.Windows.Forms.Label();
            this.lbl_Green = new System.Windows.Forms.Label();
            this.lbl_Blue = new System.Windows.Forms.Label();
            this.lbl_Yellow = new System.Windows.Forms.Label();
            this.lbl_Orange = new System.Windows.Forms.Label();
            this.lbl_Pink = new System.Windows.Forms.Label();
            this.pb_RedOff = new System.Windows.Forms.PictureBox();
            this.pb_GreenOff = new System.Windows.Forms.PictureBox();
            this.pb_BlueOff = new System.Windows.Forms.PictureBox();
            this.pb_YellowOff = new System.Windows.Forms.PictureBox();
            this.pb_OrangeOff = new System.Windows.Forms.PictureBox();
            this.pb_Pink = new System.Windows.Forms.PictureBox();
            this.pb_PinkOn = new System.Windows.Forms.PictureBox();
            this.pb_OrangeOn = new System.Windows.Forms.PictureBox();
            this.pb_YellowOn = new System.Windows.Forms.PictureBox();
            this.pb_BlueOn = new System.Windows.Forms.PictureBox();
            this.pb_GreenOn = new System.Windows.Forms.PictureBox();
            this.pb_RedOn = new System.Windows.Forms.PictureBox();
            this.cmdLine = new ZBobb.AlphaBlendTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_Purple = new System.Windows.Forms.Label();
            this.pb_PurpleOn = new System.Windows.Forms.PictureBox();
            this.pb_PurpleOff = new System.Windows.Forms.PictureBox();
            this.lbl_Black = new System.Windows.Forms.Label();
            this.pb_BlackOn = new System.Windows.Forms.PictureBox();
            this.pb_BlackOff = new System.Windows.Forms.PictureBox();
            this.lbl_White = new System.Windows.Forms.Label();
            this.pb_WhiteOn = new System.Windows.Forms.PictureBox();
            this.pb_WhiteOff = new System.Windows.Forms.PictureBox();
            this.Brown = new System.Windows.Forms.Label();
            this.pb_BrownOn = new System.Windows.Forms.PictureBox();
            this.pb_BrownOff = new System.Windows.Forms.PictureBox();
            this.lbl_Other = new System.Windows.Forms.Label();
            this.pb_OtherOn = new System.Windows.Forms.PictureBox();
            this.pb_OtherOff = new System.Windows.Forms.PictureBox();
            this.lbl_CurrentWindow = new System.Windows.Forms.Label();
            this.outputWindow = new ZBobb.AlphaBlendTextBox();
            this.outputWindowBackground = new System.Windows.Forms.Panel();
            this.pb_fillOn = new System.Windows.Forms.PictureBox();
            this.pb_fillOff = new System.Windows.Forms.PictureBox();
            this.lbl_fillOnOff = new System.Windows.Forms.Label();
            this.pb_flickSwitchUp = new System.Windows.Forms.PictureBox();
            this.pb_flickSwitchDown = new System.Windows.Forms.PictureBox();
            this.flipSwitchTimer = new System.Windows.Forms.Timer(this.components);
            this.lbl_debug = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Canvas)).BeginInit();
            this.consoleBackground.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.redLightOffCmd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redLightOnCmd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redLightOnScript)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.redLightOffScript)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_buttonDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_RedOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_GreenOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BlueOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_YellowOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_OrangeOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Pink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_PinkOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_OrangeOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_YellowOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BlueOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_GreenOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_RedOn)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_PurpleOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_PurpleOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BlackOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BlackOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_WhiteOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_WhiteOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BrownOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BrownOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_OtherOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_OtherOff)).BeginInit();
            this.outputWindowBackground.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_fillOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_fillOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_flickSwitchUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_flickSwitchDown)).BeginInit();
            this.SuspendLayout();
            // 
            // pb_Canvas
            // 
            this.pb_Canvas.BackColor = System.Drawing.Color.White;
            this.pb_Canvas.Location = new System.Drawing.Point(650, 14);
            this.pb_Canvas.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pb_Canvas.Name = "pb_Canvas";
            this.pb_Canvas.Size = new System.Drawing.Size(585, 433);
            this.pb_Canvas.TabIndex = 2;
            this.pb_Canvas.TabStop = false;
            this.pb_Canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.pb_Canvas_Paint);
            // 
            // lbl_activeWindow
            // 
            this.lbl_activeWindow.AutoSize = true;
            this.lbl_activeWindow.BackColor = System.Drawing.Color.Transparent;
            this.lbl_activeWindow.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_activeWindow.Location = new System.Drawing.Point(874, 538);
            this.lbl_activeWindow.Name = "lbl_activeWindow";
            this.lbl_activeWindow.Size = new System.Drawing.Size(97, 21);
            this.lbl_activeWindow.TabIndex = 5;
            this.lbl_activeWindow.Text = "Command Line";
            // 
            // bu_Execute
            // 
            this.bu_Execute.BackColor = System.Drawing.Color.Transparent;
            this.bu_Execute.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bu_Execute.BackgroundImage")));
            this.bu_Execute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bu_Execute.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bu_Execute.ForeColor = System.Drawing.Color.Black;
            this.bu_Execute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bu_Execute.Location = new System.Drawing.Point(493, 562);
            this.bu_Execute.Margin = new System.Windows.Forms.Padding(0);
            this.bu_Execute.Name = "bu_Execute";
            this.bu_Execute.Size = new System.Drawing.Size(120, 120);
            this.bu_Execute.TabIndex = 7;
            this.bu_Execute.UseVisualStyleBackColor = false;
            this.bu_Execute.Click += new System.EventHandler(this.bu_Execute_Click);
            // 
            // consoleBackground
            // 
            this.consoleBackground.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("consoleBackground.BackgroundImage")));
            this.consoleBackground.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.consoleBackground.Controls.Add(this.codeWindow);
            this.consoleBackground.Location = new System.Drawing.Point(14, 14);
            this.consoleBackground.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.consoleBackground.Name = "consoleBackground";
            this.consoleBackground.Size = new System.Drawing.Size(614, 433);
            this.consoleBackground.TabIndex = 12;
            // 
            // codeWindow
            // 
            this.codeWindow.BackAlpha = 10;
            this.codeWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(20)))), ((int)(((byte)(0)))));
            this.codeWindow.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.codeWindow.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeWindow.ForeColor = System.Drawing.Color.DarkGreen;
            this.codeWindow.Location = new System.Drawing.Point(32, 28);
            this.codeWindow.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.codeWindow.Multiline = true;
            this.codeWindow.Name = "codeWindow";
            this.codeWindow.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.codeWindow.Size = new System.Drawing.Size(555, 386);
            this.codeWindow.TabIndex = 13;
            this.codeWindow.Enter += new System.EventHandler(this.codeWindow_Enter);
            // 
            // bu_clearcode2
            // 
            this.bu_clearcode2.BackColor = System.Drawing.Color.DarkGray;
            this.bu_clearcode2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bu_clearcode2.BackgroundImage")));
            this.bu_clearcode2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bu_clearcode2.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bu_clearcode2.FlatAppearance.BorderSize = 0;
            this.bu_clearcode2.Location = new System.Drawing.Point(16, 473);
            this.bu_clearcode2.Margin = new System.Windows.Forms.Padding(0);
            this.bu_clearcode2.Name = "bu_clearcode2";
            this.bu_clearcode2.Size = new System.Drawing.Size(34, 45);
            this.bu_clearcode2.TabIndex = 4;
            this.bu_clearcode2.UseVisualStyleBackColor = false;
            this.bu_clearcode2.Click += new System.EventHandler(this.bu_clearcode2_Click);
            // 
            // bu_clearcode1
            // 
            this.bu_clearcode1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bu_clearcode1.BackgroundImage")));
            this.bu_clearcode1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bu_clearcode1.Location = new System.Drawing.Point(16, 473);
            this.bu_clearcode1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bu_clearcode1.Name = "bu_clearcode1";
            this.bu_clearcode1.Size = new System.Drawing.Size(34, 45);
            this.bu_clearcode1.TabIndex = 3;
            this.bu_clearcode1.UseVisualStyleBackColor = true;
            this.bu_clearcode1.Click += new System.EventHandler(this.bu_clearcode1_Click);
            // 
            // redLightOffCmd
            // 
            this.redLightOffCmd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("redLightOffCmd.BackgroundImage")));
            this.redLightOffCmd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.redLightOffCmd.Location = new System.Drawing.Point(995, 534);
            this.redLightOffCmd.Name = "redLightOffCmd";
            this.redLightOffCmd.Size = new System.Drawing.Size(23, 25);
            this.redLightOffCmd.TabIndex = 14;
            this.redLightOffCmd.TabStop = false;
            // 
            // redLightOnCmd
            // 
            this.redLightOnCmd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("redLightOnCmd.BackgroundImage")));
            this.redLightOnCmd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.redLightOnCmd.Location = new System.Drawing.Point(995, 534);
            this.redLightOnCmd.Name = "redLightOnCmd";
            this.redLightOnCmd.Size = new System.Drawing.Size(23, 25);
            this.redLightOnCmd.TabIndex = 15;
            this.redLightOnCmd.TabStop = false;
            this.redLightOnCmd.Visible = false;
            // 
            // lbl_PenColour
            // 
            this.lbl_PenColour.AutoSize = true;
            this.lbl_PenColour.BackColor = System.Drawing.Color.Transparent;
            this.lbl_PenColour.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_PenColour.Location = new System.Drawing.Point(691, 479);
            this.lbl_PenColour.Name = "lbl_PenColour";
            this.lbl_PenColour.Size = new System.Drawing.Size(110, 22);
            this.lbl_PenColour.TabIndex = 20;
            this.lbl_PenColour.Text = "Pen Colour";
            // 
            // multiLineLable
            // 
            this.multiLineLable.AutoSize = true;
            this.multiLineLable.BackColor = System.Drawing.Color.Transparent;
            this.multiLineLable.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiLineLable.Location = new System.Drawing.Point(249, 484);
            this.multiLineLable.Name = "multiLineLable";
            this.multiLineLable.Size = new System.Drawing.Size(0, 21);
            this.multiLineLable.TabIndex = 29;
            // 
            // lbl_multiLine
            // 
            this.lbl_multiLine.AutoSize = true;
            this.lbl_multiLine.BackColor = System.Drawing.Color.Transparent;
            this.lbl_multiLine.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_multiLine.Location = new System.Drawing.Point(889, 574);
            this.lbl_multiLine.Name = "lbl_multiLine";
            this.lbl_multiLine.Size = new System.Drawing.Size(82, 21);
            this.lbl_multiLine.TabIndex = 30;
            this.lbl_multiLine.Text = "Script Writer";
            // 
            // redLightOnScript
            // 
            this.redLightOnScript.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("redLightOnScript.BackgroundImage")));
            this.redLightOnScript.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.redLightOnScript.Location = new System.Drawing.Point(995, 570);
            this.redLightOnScript.Name = "redLightOnScript";
            this.redLightOnScript.Size = new System.Drawing.Size(23, 25);
            this.redLightOnScript.TabIndex = 32;
            this.redLightOnScript.TabStop = false;
            this.redLightOnScript.Visible = false;
            // 
            // redLightOffScript
            // 
            this.redLightOffScript.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("redLightOffScript.BackgroundImage")));
            this.redLightOffScript.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.redLightOffScript.Location = new System.Drawing.Point(995, 569);
            this.redLightOffScript.Name = "redLightOffScript";
            this.redLightOffScript.Size = new System.Drawing.Size(23, 25);
            this.redLightOffScript.TabIndex = 31;
            this.redLightOffScript.TabStop = false;
            // 
            // buttonTimer
            // 
            this.buttonTimer.Interval = 750;
            this.buttonTimer.Tick += new System.EventHandler(this.buttonTimer_Tick);
            // 
            // pb_buttonDown
            // 
            this.pb_buttonDown.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_buttonDown.BackgroundImage")));
            this.pb_buttonDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_buttonDown.Location = new System.Drawing.Point(497, 567);
            this.pb_buttonDown.Name = "pb_buttonDown";
            this.pb_buttonDown.Size = new System.Drawing.Size(112, 112);
            this.pb_buttonDown.TabIndex = 33;
            this.pb_buttonDown.TabStop = false;
            this.pb_buttonDown.Visible = false;
            // 
            // loadButton
            // 
            this.loadButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("loadButton.BackgroundImage")));
            this.loadButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.loadButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.loadButton.Location = new System.Drawing.Point(540, 473);
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(45, 45);
            this.loadButton.TabIndex = 37;
            this.loadButton.TabStop = false;
            this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("saveButton.BackgroundImage")));
            this.saveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.saveButton.Cursor = System.Windows.Forms.Cursors.Default;
            this.saveButton.Location = new System.Drawing.Point(585, 473);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(45, 45);
            this.saveButton.TabIndex = 38;
            this.saveButton.TabStop = false;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // lbl_Red
            // 
            this.lbl_Red.AutoSize = true;
            this.lbl_Red.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Red.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Red.Location = new System.Drawing.Point(676, 517);
            this.lbl_Red.Name = "lbl_Red";
            this.lbl_Red.Size = new System.Drawing.Size(35, 16);
            this.lbl_Red.TabIndex = 40;
            this.lbl_Red.Text = "Red";
            // 
            // lbl_Green
            // 
            this.lbl_Green.AutoSize = true;
            this.lbl_Green.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Green.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Green.Location = new System.Drawing.Point(660, 547);
            this.lbl_Green.Name = "lbl_Green";
            this.lbl_Green.Size = new System.Drawing.Size(51, 16);
            this.lbl_Green.TabIndex = 41;
            this.lbl_Green.Text = "Green";
            // 
            // lbl_Blue
            // 
            this.lbl_Blue.AutoSize = true;
            this.lbl_Blue.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Blue.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Blue.Location = new System.Drawing.Point(669, 578);
            this.lbl_Blue.Name = "lbl_Blue";
            this.lbl_Blue.Size = new System.Drawing.Size(39, 16);
            this.lbl_Blue.TabIndex = 42;
            this.lbl_Blue.Text = "Blue";
            // 
            // lbl_Yellow
            // 
            this.lbl_Yellow.AutoSize = true;
            this.lbl_Yellow.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Yellow.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Yellow.Location = new System.Drawing.Point(743, 517);
            this.lbl_Yellow.Name = "lbl_Yellow";
            this.lbl_Yellow.Size = new System.Drawing.Size(56, 16);
            this.lbl_Yellow.TabIndex = 43;
            this.lbl_Yellow.Text = "Yellow";
            // 
            // lbl_Orange
            // 
            this.lbl_Orange.AutoSize = true;
            this.lbl_Orange.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Orange.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Orange.Location = new System.Drawing.Point(738, 547);
            this.lbl_Orange.Name = "lbl_Orange";
            this.lbl_Orange.Size = new System.Drawing.Size(61, 16);
            this.lbl_Orange.TabIndex = 44;
            this.lbl_Orange.Text = "Orange";
            // 
            // lbl_Pink
            // 
            this.lbl_Pink.AutoSize = true;
            this.lbl_Pink.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Pink.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Pink.Location = new System.Drawing.Point(761, 578);
            this.lbl_Pink.Name = "lbl_Pink";
            this.lbl_Pink.Size = new System.Drawing.Size(38, 16);
            this.lbl_Pink.TabIndex = 45;
            this.lbl_Pink.Text = "Pink";
            // 
            // pb_RedOff
            // 
            this.pb_RedOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_RedOff.BackgroundImage")));
            this.pb_RedOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_RedOff.Location = new System.Drawing.Point(710, 508);
            this.pb_RedOff.Name = "pb_RedOff";
            this.pb_RedOff.Size = new System.Drawing.Size(23, 25);
            this.pb_RedOff.TabIndex = 46;
            this.pb_RedOff.TabStop = false;
            // 
            // pb_GreenOff
            // 
            this.pb_GreenOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_GreenOff.BackgroundImage")));
            this.pb_GreenOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_GreenOff.Location = new System.Drawing.Point(710, 538);
            this.pb_GreenOff.Name = "pb_GreenOff";
            this.pb_GreenOff.Size = new System.Drawing.Size(23, 25);
            this.pb_GreenOff.TabIndex = 47;
            this.pb_GreenOff.TabStop = false;
            // 
            // pb_BlueOff
            // 
            this.pb_BlueOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_BlueOff.BackgroundImage")));
            this.pb_BlueOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_BlueOff.Location = new System.Drawing.Point(710, 569);
            this.pb_BlueOff.Name = "pb_BlueOff";
            this.pb_BlueOff.Size = new System.Drawing.Size(23, 25);
            this.pb_BlueOff.TabIndex = 48;
            this.pb_BlueOff.TabStop = false;
            // 
            // pb_YellowOff
            // 
            this.pb_YellowOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_YellowOff.BackgroundImage")));
            this.pb_YellowOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_YellowOff.Location = new System.Drawing.Point(799, 508);
            this.pb_YellowOff.Name = "pb_YellowOff";
            this.pb_YellowOff.Size = new System.Drawing.Size(23, 25);
            this.pb_YellowOff.TabIndex = 49;
            this.pb_YellowOff.TabStop = false;
            // 
            // pb_OrangeOff
            // 
            this.pb_OrangeOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_OrangeOff.BackgroundImage")));
            this.pb_OrangeOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_OrangeOff.Location = new System.Drawing.Point(799, 538);
            this.pb_OrangeOff.Name = "pb_OrangeOff";
            this.pb_OrangeOff.Size = new System.Drawing.Size(23, 25);
            this.pb_OrangeOff.TabIndex = 50;
            this.pb_OrangeOff.TabStop = false;
            // 
            // pb_Pink
            // 
            this.pb_Pink.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_Pink.BackgroundImage")));
            this.pb_Pink.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_Pink.Location = new System.Drawing.Point(799, 569);
            this.pb_Pink.Name = "pb_Pink";
            this.pb_Pink.Size = new System.Drawing.Size(23, 25);
            this.pb_Pink.TabIndex = 51;
            this.pb_Pink.TabStop = false;
            // 
            // pb_PinkOn
            // 
            this.pb_PinkOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_PinkOn.BackgroundImage")));
            this.pb_PinkOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_PinkOn.Location = new System.Drawing.Point(799, 569);
            this.pb_PinkOn.Name = "pb_PinkOn";
            this.pb_PinkOn.Size = new System.Drawing.Size(23, 25);
            this.pb_PinkOn.TabIndex = 57;
            this.pb_PinkOn.TabStop = false;
            this.pb_PinkOn.Visible = false;
            // 
            // pb_OrangeOn
            // 
            this.pb_OrangeOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_OrangeOn.BackgroundImage")));
            this.pb_OrangeOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_OrangeOn.Location = new System.Drawing.Point(799, 538);
            this.pb_OrangeOn.Name = "pb_OrangeOn";
            this.pb_OrangeOn.Size = new System.Drawing.Size(23, 25);
            this.pb_OrangeOn.TabIndex = 56;
            this.pb_OrangeOn.TabStop = false;
            this.pb_OrangeOn.Visible = false;
            // 
            // pb_YellowOn
            // 
            this.pb_YellowOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_YellowOn.BackgroundImage")));
            this.pb_YellowOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_YellowOn.Location = new System.Drawing.Point(799, 508);
            this.pb_YellowOn.Name = "pb_YellowOn";
            this.pb_YellowOn.Size = new System.Drawing.Size(23, 25);
            this.pb_YellowOn.TabIndex = 55;
            this.pb_YellowOn.TabStop = false;
            this.pb_YellowOn.Visible = false;
            // 
            // pb_BlueOn
            // 
            this.pb_BlueOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_BlueOn.BackgroundImage")));
            this.pb_BlueOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_BlueOn.Location = new System.Drawing.Point(710, 570);
            this.pb_BlueOn.Name = "pb_BlueOn";
            this.pb_BlueOn.Size = new System.Drawing.Size(23, 25);
            this.pb_BlueOn.TabIndex = 54;
            this.pb_BlueOn.TabStop = false;
            this.pb_BlueOn.Visible = false;
            // 
            // pb_GreenOn
            // 
            this.pb_GreenOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_GreenOn.BackgroundImage")));
            this.pb_GreenOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_GreenOn.Location = new System.Drawing.Point(710, 538);
            this.pb_GreenOn.Name = "pb_GreenOn";
            this.pb_GreenOn.Size = new System.Drawing.Size(23, 25);
            this.pb_GreenOn.TabIndex = 53;
            this.pb_GreenOn.TabStop = false;
            this.pb_GreenOn.Visible = false;
            // 
            // pb_RedOn
            // 
            this.pb_RedOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_RedOn.BackgroundImage")));
            this.pb_RedOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_RedOn.Location = new System.Drawing.Point(710, 508);
            this.pb_RedOn.Name = "pb_RedOn";
            this.pb_RedOn.Size = new System.Drawing.Size(23, 25);
            this.pb_RedOn.TabIndex = 52;
            this.pb_RedOn.TabStop = false;
            this.pb_RedOn.Visible = false;
            // 
            // cmdLine
            // 
            this.cmdLine.BackAlpha = 10;
            this.cmdLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cmdLine.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cmdLine.Location = new System.Drawing.Point(15, 9);
            this.cmdLine.Name = "cmdLine";
            this.cmdLine.Size = new System.Drawing.Size(432, 16);
            this.cmdLine.TabIndex = 58;
            this.cmdLine.Enter += new System.EventHandler(this.cmdLine_Enter);
            this.cmdLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmdLine_KeyDown);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.cmdLine);
            this.panel1.Location = new System.Drawing.Point(67, 479);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(461, 35);
            this.panel1.TabIndex = 59;
            // 
            // lbl_Purple
            // 
            this.lbl_Purple.AutoSize = true;
            this.lbl_Purple.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Purple.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Purple.Location = new System.Drawing.Point(654, 612);
            this.lbl_Purple.Name = "lbl_Purple";
            this.lbl_Purple.Size = new System.Drawing.Size(54, 16);
            this.lbl_Purple.TabIndex = 60;
            this.lbl_Purple.Text = "Purple";
            // 
            // pb_PurpleOn
            // 
            this.pb_PurpleOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_PurpleOn.BackgroundImage")));
            this.pb_PurpleOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_PurpleOn.Location = new System.Drawing.Point(710, 603);
            this.pb_PurpleOn.Name = "pb_PurpleOn";
            this.pb_PurpleOn.Size = new System.Drawing.Size(23, 25);
            this.pb_PurpleOn.TabIndex = 61;
            this.pb_PurpleOn.TabStop = false;
            this.pb_PurpleOn.Visible = false;
            // 
            // pb_PurpleOff
            // 
            this.pb_PurpleOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_PurpleOff.BackgroundImage")));
            this.pb_PurpleOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_PurpleOff.Location = new System.Drawing.Point(710, 603);
            this.pb_PurpleOff.Name = "pb_PurpleOff";
            this.pb_PurpleOff.Size = new System.Drawing.Size(23, 25);
            this.pb_PurpleOff.TabIndex = 62;
            this.pb_PurpleOff.TabStop = false;
            // 
            // lbl_Black
            // 
            this.lbl_Black.AutoSize = true;
            this.lbl_Black.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Black.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Black.Location = new System.Drawing.Point(752, 612);
            this.lbl_Black.Name = "lbl_Black";
            this.lbl_Black.Size = new System.Drawing.Size(47, 16);
            this.lbl_Black.TabIndex = 63;
            this.lbl_Black.Text = "Black";
            // 
            // pb_BlackOn
            // 
            this.pb_BlackOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_BlackOn.BackgroundImage")));
            this.pb_BlackOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_BlackOn.Location = new System.Drawing.Point(799, 603);
            this.pb_BlackOn.Name = "pb_BlackOn";
            this.pb_BlackOn.Size = new System.Drawing.Size(23, 25);
            this.pb_BlackOn.TabIndex = 64;
            this.pb_BlackOn.TabStop = false;
            this.pb_BlackOn.Visible = false;
            // 
            // pb_BlackOff
            // 
            this.pb_BlackOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_BlackOff.BackgroundImage")));
            this.pb_BlackOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_BlackOff.Location = new System.Drawing.Point(799, 603);
            this.pb_BlackOff.Name = "pb_BlackOff";
            this.pb_BlackOff.Size = new System.Drawing.Size(23, 25);
            this.pb_BlackOff.TabIndex = 65;
            this.pb_BlackOff.TabStop = false;
            // 
            // lbl_White
            // 
            this.lbl_White.AutoSize = true;
            this.lbl_White.BackColor = System.Drawing.Color.Transparent;
            this.lbl_White.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_White.Location = new System.Drawing.Point(654, 644);
            this.lbl_White.Name = "lbl_White";
            this.lbl_White.Size = new System.Drawing.Size(51, 16);
            this.lbl_White.TabIndex = 66;
            this.lbl_White.Text = "White";
            // 
            // pb_WhiteOn
            // 
            this.pb_WhiteOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_WhiteOn.BackgroundImage")));
            this.pb_WhiteOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_WhiteOn.Location = new System.Drawing.Point(710, 635);
            this.pb_WhiteOn.Name = "pb_WhiteOn";
            this.pb_WhiteOn.Size = new System.Drawing.Size(23, 25);
            this.pb_WhiteOn.TabIndex = 67;
            this.pb_WhiteOn.TabStop = false;
            this.pb_WhiteOn.Visible = false;
            // 
            // pb_WhiteOff
            // 
            this.pb_WhiteOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_WhiteOff.BackgroundImage")));
            this.pb_WhiteOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_WhiteOff.Location = new System.Drawing.Point(710, 635);
            this.pb_WhiteOff.Name = "pb_WhiteOff";
            this.pb_WhiteOff.Size = new System.Drawing.Size(23, 25);
            this.pb_WhiteOff.TabIndex = 68;
            this.pb_WhiteOff.TabStop = false;
            // 
            // Brown
            // 
            this.Brown.AutoSize = true;
            this.Brown.BackColor = System.Drawing.Color.Transparent;
            this.Brown.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Brown.Location = new System.Drawing.Point(746, 644);
            this.Brown.Name = "Brown";
            this.Brown.Size = new System.Drawing.Size(53, 16);
            this.Brown.TabIndex = 69;
            this.Brown.Text = "Brown";
            // 
            // pb_BrownOn
            // 
            this.pb_BrownOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_BrownOn.BackgroundImage")));
            this.pb_BrownOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_BrownOn.Location = new System.Drawing.Point(799, 634);
            this.pb_BrownOn.Name = "pb_BrownOn";
            this.pb_BrownOn.Size = new System.Drawing.Size(23, 25);
            this.pb_BrownOn.TabIndex = 70;
            this.pb_BrownOn.TabStop = false;
            this.pb_BrownOn.Visible = false;
            // 
            // pb_BrownOff
            // 
            this.pb_BrownOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_BrownOff.BackgroundImage")));
            this.pb_BrownOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_BrownOff.Location = new System.Drawing.Point(799, 635);
            this.pb_BrownOff.Name = "pb_BrownOff";
            this.pb_BrownOff.Size = new System.Drawing.Size(23, 25);
            this.pb_BrownOff.TabIndex = 71;
            this.pb_BrownOff.TabStop = false;
            // 
            // lbl_Other
            // 
            this.lbl_Other.AutoSize = true;
            this.lbl_Other.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Other.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Other.Location = new System.Drawing.Point(654, 678);
            this.lbl_Other.Name = "lbl_Other";
            this.lbl_Other.Size = new System.Drawing.Size(50, 16);
            this.lbl_Other.TabIndex = 72;
            this.lbl_Other.Text = "Other";
            // 
            // pb_OtherOn
            // 
            this.pb_OtherOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_OtherOn.BackgroundImage")));
            this.pb_OtherOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_OtherOn.Location = new System.Drawing.Point(710, 668);
            this.pb_OtherOn.Name = "pb_OtherOn";
            this.pb_OtherOn.Size = new System.Drawing.Size(23, 25);
            this.pb_OtherOn.TabIndex = 73;
            this.pb_OtherOn.TabStop = false;
            this.pb_OtherOn.Visible = false;
            // 
            // pb_OtherOff
            // 
            this.pb_OtherOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_OtherOff.BackgroundImage")));
            this.pb_OtherOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_OtherOff.Location = new System.Drawing.Point(710, 668);
            this.pb_OtherOff.Name = "pb_OtherOff";
            this.pb_OtherOff.Size = new System.Drawing.Size(23, 25);
            this.pb_OtherOff.TabIndex = 74;
            this.pb_OtherOff.TabStop = false;
            // 
            // lbl_CurrentWindow
            // 
            this.lbl_CurrentWindow.AutoSize = true;
            this.lbl_CurrentWindow.BackColor = System.Drawing.Color.Transparent;
            this.lbl_CurrentWindow.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CurrentWindow.Location = new System.Drawing.Point(892, 479);
            this.lbl_CurrentWindow.Name = "lbl_CurrentWindow";
            this.lbl_CurrentWindow.Size = new System.Drawing.Size(140, 22);
            this.lbl_CurrentWindow.TabIndex = 75;
            this.lbl_CurrentWindow.Text = "Active Window";
            // 
            // outputWindow
            // 
            this.outputWindow.BackAlpha = 10;
            this.outputWindow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.outputWindow.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.outputWindow.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.outputWindow.Location = new System.Drawing.Point(10, 8);
            this.outputWindow.Multiline = true;
            this.outputWindow.Name = "outputWindow";
            this.outputWindow.ReadOnly = true;
            this.outputWindow.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputWindow.Size = new System.Drawing.Size(385, 115);
            this.outputWindow.TabIndex = 76;
            // 
            // outputWindowBackground
            // 
            this.outputWindowBackground.BackColor = System.Drawing.Color.Azure;
            this.outputWindowBackground.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("outputWindowBackground.BackgroundImage")));
            this.outputWindowBackground.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.outputWindowBackground.Controls.Add(this.outputWindow);
            this.outputWindowBackground.Location = new System.Drawing.Point(27, 562);
            this.outputWindowBackground.Name = "outputWindowBackground";
            this.outputWindowBackground.Size = new System.Drawing.Size(406, 131);
            this.outputWindowBackground.TabIndex = 77;
            // 
            // pb_fillOn
            // 
            this.pb_fillOn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_fillOn.BackgroundImage")));
            this.pb_fillOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_fillOn.Location = new System.Drawing.Point(799, 669);
            this.pb_fillOn.Name = "pb_fillOn";
            this.pb_fillOn.Size = new System.Drawing.Size(23, 25);
            this.pb_fillOn.TabIndex = 78;
            this.pb_fillOn.TabStop = false;
            this.pb_fillOn.Visible = false;
            // 
            // pb_fillOff
            // 
            this.pb_fillOff.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_fillOff.BackgroundImage")));
            this.pb_fillOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_fillOff.Location = new System.Drawing.Point(799, 669);
            this.pb_fillOff.Name = "pb_fillOff";
            this.pb_fillOff.Size = new System.Drawing.Size(23, 25);
            this.pb_fillOff.TabIndex = 79;
            this.pb_fillOff.TabStop = false;
            // 
            // lbl_fillOnOff
            // 
            this.lbl_fillOnOff.AutoSize = true;
            this.lbl_fillOnOff.BackColor = System.Drawing.Color.Transparent;
            this.lbl_fillOnOff.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_fillOnOff.Location = new System.Drawing.Point(769, 678);
            this.lbl_fillOnOff.Name = "lbl_fillOnOff";
            this.lbl_fillOnOff.Size = new System.Drawing.Size(29, 16);
            this.lbl_fillOnOff.TabIndex = 80;
            this.lbl_fillOnOff.Text = "Fill";
            // 
            // pb_flickSwitchUp
            // 
            this.pb_flickSwitchUp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_flickSwitchUp.BackgroundImage")));
            this.pb_flickSwitchUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_flickSwitchUp.Location = new System.Drawing.Point(995, 624);
            this.pb_flickSwitchUp.Name = "pb_flickSwitchUp";
            this.pb_flickSwitchUp.Size = new System.Drawing.Size(23, 40);
            this.pb_flickSwitchUp.TabIndex = 81;
            this.pb_flickSwitchUp.TabStop = false;
            this.pb_flickSwitchUp.Click += new System.EventHandler(this.pb_flickSwitchUp_Click);
            // 
            // pb_flickSwitchDown
            // 
            this.pb_flickSwitchDown.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pb_flickSwitchDown.BackgroundImage")));
            this.pb_flickSwitchDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pb_flickSwitchDown.Location = new System.Drawing.Point(995, 624);
            this.pb_flickSwitchDown.Name = "pb_flickSwitchDown";
            this.pb_flickSwitchDown.Size = new System.Drawing.Size(23, 40);
            this.pb_flickSwitchDown.TabIndex = 82;
            this.pb_flickSwitchDown.TabStop = false;
            // 
            // flipSwitchTimer
            // 
            this.flipSwitchTimer.Interval = 750;
            this.flipSwitchTimer.Tick += new System.EventHandler(this.flipSwitchTimer_Tick);
            // 
            // lbl_debug
            // 
            this.lbl_debug.AutoSize = true;
            this.lbl_debug.BackColor = System.Drawing.Color.Transparent;
            this.lbl_debug.Font = new System.Drawing.Font("Franklin Gothic Medium Cond", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_debug.Location = new System.Drawing.Point(884, 631);
            this.lbl_debug.Name = "lbl_debug";
            this.lbl_debug.Size = new System.Drawing.Size(87, 21);
            this.lbl_debug.TabIndex = 83;
            this.lbl_debug.Text = "Check Syntax";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1273, 733);
            this.Controls.Add(this.lbl_debug);
            this.Controls.Add(this.pb_flickSwitchUp);
            this.Controls.Add(this.pb_flickSwitchDown);
            this.Controls.Add(this.bu_clearcode2);
            this.Controls.Add(this.lbl_fillOnOff);
            this.Controls.Add(this.pb_fillOn);
            this.Controls.Add(this.pb_fillOff);
            this.Controls.Add(this.outputWindowBackground);
            this.Controls.Add(this.lbl_CurrentWindow);
            this.Controls.Add(this.pb_OtherOn);
            this.Controls.Add(this.lbl_Other);
            this.Controls.Add(this.Brown);
            this.Controls.Add(this.pb_WhiteOn);
            this.Controls.Add(this.lbl_White);
            this.Controls.Add(this.pb_BlackOn);
            this.Controls.Add(this.lbl_Black);
            this.Controls.Add(this.pb_PurpleOn);
            this.Controls.Add(this.pb_PurpleOff);
            this.Controls.Add(this.lbl_Purple);
            this.Controls.Add(this.pb_PinkOn);
            this.Controls.Add(this.pb_OrangeOn);
            this.Controls.Add(this.pb_YellowOn);
            this.Controls.Add(this.pb_BlueOn);
            this.Controls.Add(this.pb_GreenOn);
            this.Controls.Add(this.pb_RedOn);
            this.Controls.Add(this.pb_Pink);
            this.Controls.Add(this.pb_OrangeOff);
            this.Controls.Add(this.pb_YellowOff);
            this.Controls.Add(this.pb_BlueOff);
            this.Controls.Add(this.pb_GreenOff);
            this.Controls.Add(this.pb_RedOff);
            this.Controls.Add(this.lbl_Pink);
            this.Controls.Add(this.lbl_Orange);
            this.Controls.Add(this.lbl_Yellow);
            this.Controls.Add(this.lbl_Blue);
            this.Controls.Add(this.lbl_Green);
            this.Controls.Add(this.lbl_Red);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.loadButton);
            this.Controls.Add(this.redLightOnScript);
            this.Controls.Add(this.redLightOffScript);
            this.Controls.Add(this.lbl_multiLine);
            this.Controls.Add(this.multiLineLable);
            this.Controls.Add(this.lbl_PenColour);
            this.Controls.Add(this.redLightOnCmd);
            this.Controls.Add(this.redLightOffCmd);
            this.Controls.Add(this.consoleBackground);
            this.Controls.Add(this.lbl_activeWindow);
            this.Controls.Add(this.bu_clearcode1);
            this.Controls.Add(this.pb_Canvas);
            this.Controls.Add(this.pb_buttonDown);
            this.Controls.Add(this.bu_Execute);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pb_BlackOff);
            this.Controls.Add(this.pb_WhiteOff);
            this.Controls.Add(this.pb_BrownOn);
            this.Controls.Add(this.pb_BrownOff);
            this.Controls.Add(this.pb_OtherOff);
            this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(1289, 772);
            this.MinimumSize = new System.Drawing.Size(1289, 772);
            this.Name = "MainForm";
            this.Text = "EasyDraw";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb_Canvas)).EndInit();
            this.consoleBackground.ResumeLayout(false);
            this.consoleBackground.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.redLightOffCmd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redLightOnCmd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redLightOnScript)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.redLightOffScript)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_buttonDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loadButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saveButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_RedOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_GreenOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BlueOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_YellowOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_OrangeOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_Pink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_PinkOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_OrangeOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_YellowOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BlueOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_GreenOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_RedOn)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_PurpleOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_PurpleOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BlackOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BlackOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_WhiteOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_WhiteOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BrownOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_BrownOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_OtherOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_OtherOff)).EndInit();
            this.outputWindowBackground.ResumeLayout(false);
            this.outputWindowBackground.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_fillOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_fillOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_flickSwitchUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb_flickSwitchDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label lbl_activeWindow;
        private System.Windows.Forms.Button bu_Execute;
        private Panel consoleBackground;

        public MainForm(AlphaBlendTextBox codeWindow)
        {
            this.codeWindow = codeWindow;
           
        }

        private Button bu_clearcode2;
        private Button bu_clearcode1;
        private PictureBox redLightOffCmd;
        private PictureBox redLightOnCmd;
        private Label lbl_PenColour;
        private Label multiLineLable;
        private Label lbl_multiLine;
        private PictureBox redLightOnScript;
        private PictureBox redLightOffScript;
        private Timer buttonTimer;
        private PictureBox pb_buttonDown;
        private PictureBox loadButton;
        private PictureBox saveButton;
        public AlphaBlendTextBox codeWindow;
        public PictureBox pb_Canvas;
        private Label lbl_Red;
        private Label lbl_Green;
        private Label lbl_Blue;
        private Label lbl_Yellow;
        private Label lbl_Orange;
        private Label lbl_Pink;
        private PictureBox pb_RedOff;
        private PictureBox pb_GreenOff;
        private PictureBox pb_BlueOff;
        private PictureBox pb_YellowOff;
        private PictureBox pb_OrangeOff;
        private PictureBox pb_Pink;
        public PictureBox pb_PinkOn;
        public PictureBox pb_OrangeOn;
        public PictureBox pb_YellowOn;
        public PictureBox pb_BlueOn;
        public PictureBox pb_GreenOn;
        public PictureBox pb_RedOn;
        private AlphaBlendTextBox cmdLine;
        private Panel panel1;
        private Label lbl_Purple;
        public PictureBox pb_PurpleOn;
        private PictureBox pb_PurpleOff;
        private Label lbl_Black;
        public PictureBox pb_BlackOn;
        private PictureBox pb_BlackOff;
        private Label lbl_White;
        public PictureBox pb_WhiteOn;
        private PictureBox pb_WhiteOff;
        private Label Brown;
        public PictureBox pb_BrownOn;
        private PictureBox pb_BrownOff;
        private Label lbl_Other;
        public PictureBox pb_OtherOn;
        private PictureBox pb_OtherOff;
        private Label lbl_CurrentWindow;
        private Panel outputWindowBackground;
        public PictureBox pb_fillOn;
        private PictureBox pb_fillOff;
        private Label lbl_fillOnOff;
        public AlphaBlendTextBox outputWindow;
        private PictureBox pb_flickSwitchUp;
        private PictureBox pb_flickSwitchDown;
        private Timer flipSwitchTimer;
        private Label lbl_debug;
    }
}

