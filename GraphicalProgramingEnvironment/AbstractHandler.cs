﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// The abstract class which holds the initial definitons for the methods involved in making a chain of responsibility design pattern
    /// </summary>
    abstract class AbstractHandler : IHandler
    {
        private IHandler _nextHandler; //Declaring a variable to hold the next handler object to be used

        /// <summary>
        /// Enables the next handler to be set
        /// </summary>
        /// <param name="handler">The next handler object to be used</param>
        /// <returns>Returns the handler object</returns>
        public IHandler SetNext(IHandler handler) //Allows the next handler to be set
        {
            this._nextHandler = handler; //Sets the variable to equal the handler object passed as an argument
            return handler; //This lets the linking to be done on one line: e.g. handler1.setnext(handler2).setnext(handler3)...
        }
        /// <summary>
        /// This method sets out the base operation for the method responsible for returning the next handler to be accessed
        /// </summary>
        /// <param name="request">The line to be checked</param>
        /// <param name="op">Instance of the 'Operation' class</param>
        /// <param name="var">Instance of the 'Variable' class</param>
        /// <param name="p">Instance of the 'Parser' class</param>
        /// <param name="method">Instance of the 'Method' class</param>
        /// <param name="args">The arguments of the command</param>
        /// <param name="run">Whether or not the program is in syntax-checking mode</param>
        /// <param name="fracturedLine">The raw line input</param>
        /// <param name="mainForm">Instance of the 'MainForm' form</param>
        /// <param name="fracturedLineValues">The values of any variables contained in the input</param>
        /// <returns>Either returns the object, or, if there are no more objects to iterate through, 'null' is returned</returns>
        public virtual object Handle(object request, Operation op, Variable var, Parser p, Methods method, string[] args, bool run, string[] fracturedLine, MainForm mainForm, string[] fracturedLineValues)
        {
            if (this._nextHandler != null) //If there are no more handlers to iterate through, return a null value, otherwise return the next handler object
            {
                return this._nextHandler.Handle(request, op, var, p, method, args, run,fracturedLine, mainForm, fracturedLineValues);
            }
            else
            {
                return null;
            }
        }
    }
}
