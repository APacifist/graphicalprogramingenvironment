﻿using System;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Parses commands and calls the relevant command from the Canvas class
    /// - By Joshua Hooper: 33532281
    /// </summary>
    /// 
    public class Parser
    {
        //Setting up the variables needed for this class
        MainForm MainForm;
        /// <summary>
        /// class to hold drawing operations
        /// </summary>
        public Canvas myCanvas;
        Factory factory = new Factory();
        Variable var;
        Operation op;
        Methods method;
        Loop looper;
        IfStatement ifStatement;
        bool isLooping;
        /// <summary>
        /// The split-up arguments of a command
        /// </summary>
        public int args;
        /// <summary>
        /// The current line being operated on
        /// </summary>
        public int counter;
        bool testingMode;
        int loopAtLine;
        /// <summary>
        /// the full string inputted
        /// </summary>
        public string line;
        /// <summary>
        /// Whether or not to skip execution of code
        /// </summary>
        public bool skipLines;
        /// <summary>
        /// The number to skip until before resuming execution of code
        /// </summary>
        public int skipTo;
        /// <summary>
        /// The array containing the broken up line input, split by spaces, brackets and commas
        /// </summary>
        public String[] fracturedLine;
        /// <summary>
        /// The array containing the broken up line input, each cell either containing a blank space, 
        /// or the value of a input that was a variable
        /// </summary>
        public String[] fracturedLineValues;
        /// <summary>
        /// The number of errors within the program
        /// </summary>
        public int errorCount;
        /// <summary>
        /// The number of lines to be checked for executable code
        /// </summary>
        public int lineCount;
        /// <summary>
        /// Constructor for the 'Parser' class that sets up the class for normal use (not a testing environment).
        /// </summary>
        /// <param name="passedForm"></param>
        public Parser(MainForm passedForm = default)
        {
            //Assigning values to the MainForm and myCanvas variables so they can be accessed throughout the class
            MainForm = passedForm;
            myCanvas = MainForm.DrawingCanvas;
            var = (Variable)factory.GetCommand("variable");
            op = new Operation(var);
            method = (Methods)factory.GetCommand("Method");
            looper = (Loop)factory.GetCommand("Loop");
            ifStatement = (IfStatement)factory.GetCommand("if_statement");
            skipLines = false;

        }
        /// <summary>
        /// Constructor for the 'Parser' class that sets up the class for use in a testing environment. 
        /// This is neccessary as the testing environment doesn't contain any graphics capabilities, so these must be 
        /// avoided when in testing mode.
        /// </summary>
        /// <param name="testingMode"></param>
        public Parser(bool testingMode = true)//Setting the argument 'testingMode' as an optional parameter
        {
            //Assigning values to the 'testingMode' and 'myCanvas' variables
            this.testingMode = testingMode;
            myCanvas = new Canvas(testingMode); //Creating a new Canvas object, passing the 'testingMode' parameter so it doesn't try to use graphics 
            var = (Variable)factory.GetCommand("variable");
            op = new Operation(var);
            method = (Methods)factory.GetCommand("Method");
            looper = (Loop)factory.GetCommand("Loop");
            ifStatement = (IfStatement)factory.GetCommand("if_statement");
            skipLines = false;
        }
        /// <summary>
        /// Takes a string (line) and splits it into a commmand and its arguments.
        /// </summary>
        /// <param name="line">The current line string inside the textbox</param>
        /// <param name="run">Whether or not the inputted code is to be checked for errors, or executed</param>
        /// <param name="lineCounter">The current line that the program is analysing</param>
        /// <param name="looping">Whether or not the method has been called within a loop</param>
        public void ParseCommand(String line, bool run = true, int lineCounter = 0, bool looping = false)
        {
            isLooping = looping;//Allows other methods to be informed that they are being run inside a loop
            if(lineCounter != 0) //As long as the 'lineCounter' value isnt the default (0), then set the public variable to be its value
            {
                lineCount = lineCounter;
            }
            if(!isLooping) /*If the program is looping, don't incrememt 'counter' - 'pauses' the value so it can continue from 
                            *where it was after the loop has run its course, like an anchor point for the program.*/
            {
                counter += 1;//Keeps track of what line the program is on
            }
            
            if(((skipLines) & (looping) & ((lineCount) <= skipTo)))/*This is how my program 'ignores' looping code when it needs to skipped
                                                                    * e.g. when a loop/if condition are false, or a method is declared*/
            {
                Console.WriteLine("Line Count = " + lineCount + " Skip target = " + skipTo); //Debugging purposes only
            }
 
            else if ((!looping)&(skipLines) & ((counter) <= skipTo))/*This is how my program 'ignores' lines of  code when it needs to skipped
                                                                    * e.g. when a loop/if condition are false, or a method is declared*/
            /*If skiplines mode is active, and there is a value to skip to, 
              *skip running the below code until the 'counter' value matches 
              *the value to skip to*/
            {
                
                Console.WriteLine("Counter = " + counter + " Skip target = " + skipTo); //Debugging purposes only
                Console.WriteLine("Line Count = " + lineCount + " Skip target = " + skipTo); //Debugging purposes only
                Console.WriteLine("Skipping Line)"); //Debugging purposes only
            }
            else
            {
                skipLines = false; //If this block of code is running, then skiplines should be false

                this.line = line; //Assigning the passed 'line' string to the variable belonging to this class

                var bracketSplit = line.Split(')'); //This enables bracketted operations to function correctly
                fracturedLine = bracketSplit[0].Split(',', ' ', '('); ; //Splits the string by whitespace, commas or open brackets
                args = fracturedLine.Length; //Gets the of entities inside the fracturedLine array
                Console.WriteLine("Args = " + args); //Debugging purposes only

                if (fracturedLine.Length != 0) //If the array contains values then...
                {
                    fracturedLineValues = new string[fracturedLine.Length]; //Create a new array, with the same length as the fracturedLine array

                    for (int i = 0; i < fracturedLine.Length; i++) /* Loop through the 'fracturedLine' array, if the value at the current
                                                                      index is a variable, find its value and assign it to the matching index
                                                                      inside the new array, otherwise, assign it just as it is  */
                    {
                        String tempVar = var.Get(fracturedLine[i]); //Treat the current value as a variable, try to retrieve its value
                        if (tempVar.Equals("false")) //If it isn't a variable, it will return false
                        {
                            fracturedLineValues[i] = fracturedLine[i]; //Assigns the current value to the new array
                        }
                        else
                        {
                            Console.WriteLine("TempVar = " + tempVar);//Debugging purposes only

                            fracturedLineValues[i] = tempVar;// If it is a variable, assign that variable's value to the new array
                        }
                        Console.WriteLine("Print test: " + fracturedLineValues[i]);//Debugging purposes only
                    }
                    if(testingMode)//Only run non-graphical commands if testingMode is true
                    {
                        executeNonGraphicalCommands(counter, run, arg: fracturedLineValues);
                        //Calls the 'executeCommand' method, passing the counter variable, run boolean, and the new array as parameters
                    }
                    else
                    {
                        executeCommand(counter, run, arg: fracturedLineValues);
                        //Calls the 'executeCommand' method, passing the counter variable, run boolean, and the new array as parameters
                    }

                }
            }
        }
        /// <summary>
        /// Takes the formated command and arguments passed to it and calls the relevant Canvas command for them
        /// </summary>
        /// <param name="run">Whether or not the method has been called in syntax mode</param>
        /// <param name="counter">The line number of the command</param>
        /// <param name="arg">The command and arguments to be executed</param>
        private void executeCommand(int counter, bool run = true, params string[] arg)
        { 
            Console.WriteLine("Run state is: " + run);
            try/*Many errors could arise from this method, and these are caught by this enveloping try*/
            {
                Console.WriteLine("CURRENT LINE: " + line);
                Console.WriteLine("Current Match: " + op.Matches(line));

                //Declaring the variables needed for the 'chain of responsiblity' design pattern
                //These are instances of the handler objects
                var methodCallHandler = new Op_With_AssignmentHandler();
                var declarationHandler = new DeclarationHandler();
                var opWithAssignmentHandler = new MethodCallHandler();
                var emptyLineHandler = new EmptyLineHandler();
                //The below line sets up the chain, 'linking' the handler objects using the 'SEtNext' method so that each one is checked
                methodCallHandler.SetNext(opWithAssignmentHandler).SetNext(declarationHandler).SetNext(emptyLineHandler);
                //If the below method returns 'false', then there were no matches in the handler objects for the given line, so run the 'switch' statement
                if(!Client.ClientCode(methodCallHandler, line, op, var, this, method, arg, run, fracturedLine, MainForm, fracturedLineValues))
                {
                    switch (arg[0])//Each command have a switch case, if there isn't a case for it, it's an invalid command
                    {
                        case "def":
                            if (op.Matches(line).Equals("Method_Declaration") == true) //If the format isn't right for a method definition, throw an exception
                            {

                                Console.WriteLine("Recognised Method Declaration. Command: " + arg[1]);//Debuggin purposes only
                                method.DefinineMethod(MainForm, this, run, arg[1], counter.ToString()); //Call the 'DefineMethod' method from the 'method' class
                            }
                            else
                            {
                                throw new Exception("Invalid format for method definition, use: def [Method_Name]()");
                            }
                            break;
                        case "if":
                            Exception exceptIF = new Exception("Invalid command format, use: if([val/var] [operation] [val/var])"); //Defines a new exception to be thrown
                            if (op.Matches(line).Equals("IF_Statement")) //If the line doesn't match the format for an 'if' statement, throw the previously defined exception
                            {
                                if (arg.Length == 4) //The format can be matched without the correct number of arguments (whitespace usually does this)
                                {                    //So check the numeber of arguments to avoid errors
                                    Console.WriteLine("Command recognised as an IF statement");//Debugging purposes only
                                    if (!run)/*Since the IF statement wouldn't be run if in testing mode, this section has to manually check for 
                                                an enclosing 'endif' tag - done inside the method of the 'IfStatement' method*/
                                    {
                                        if (var.FindStr(MainForm, "endif", counter) == -1) //If the string can't be found, -1 is returned, so throw an excpetion
                                        {
                                            throw new Exception("ERROR: Missing enclosing 'endif'");
                                        }

                                    }
                                    else //If the program is in normal running mode, set the parameters of the 'IF' statement, then execute it
                                    {

                                        ifStatement.Set(run, arg[1], arg[2], arg[3]); //Sets the parameters of the 'IF' statement
                                        if (isLooping)
                                        {
                                            ifStatement.ExecuteIf(MainForm, var, this, count: lineCount); //Calls the 'ExecuteIf' method to make the 'IF' statement function
                                        }
                                        else
                                        {
                                            ifStatement.ExecuteIf(MainForm, var, this, count: lineCount); //Calls the 'ExecuteIf' method to make the 'IF' statement function
                                        }
                                        }
                                       
                                }
                                else //If there are less or more than four arguments, throw an exception
                                {
                                    throw exceptIF;
                                }
                            }
                            else //If the line doesn't match the format for an 'IF' statement, throw an exception
                            {
                                throw exceptIF;
                            }
                            break;
                        case "endif": //Ensures the program doesn't mistake 'endif' for an invalid command, even though it only serves as marker for the 'if' statment
                            break;
                        case "clear": //Will do nothing if in syntax mode and will clear the screen otherwise
                            Console.WriteLine("clear");//Ignore - Debugging Purposes Only
                            if (run)
                            {
                                myCanvas.ClearScreen();//Calls the method in canvas to perform the command
                            }
                            break;
                        case "center": //Will do nothing if in syntax mode and will execute a method otherwise
                            if (run)
                            {
                                myCanvas.Center();//Calls the method in canvas to perform the command
                            }
                            break;
                        case "reset":
                            if (run) //Will do nothing if in syntax mode and will execute a method otherwise
                            {
                                Console.WriteLine("reset");//Ignore - Debugging Purposes Only
                                myCanvas.ResetPos();//Calls the method in canvas to perform the command
                            }
                            break;
                        case "enddef"://Will do nothing if in syntax mode and will execute a method otherwise
                            break;
                        case "while": /*
                                       * Sets the parameters of the command, like start point, and the operation to peform,
                                       * as well as the values to perform it on
                                       * 
                                       * It then calls the 'ExecuteLoop' method from the 'looper' class, running the 'while' loop
                                       */

                            if (arg.Length == 4) //Makes sure the correct number of arguments are present, throws an exception if not
                            {
                                if (op.Matches(line).Equals("While_Loop")) //Checks the format of the line is correct, again, throwing an exception if not
                                {
                                    if (op.CheckOp(arg[2]) == true & (arg[2].Equals("=") == false)) //<ales sure the user doesn't try an assigment inside the loop definition
                                    {
                                        if (run) //If in syntax mode, don't run any functional code, otherwise, run it all
                                        {
                                            Console.WriteLine("Loop match successful");//Debugging purposes only
                                            loopAtLine = (counter - 1); //Sets the tag to let the loop know where to start looping
                                            Console.WriteLine("Loop tag found at line: " + loopAtLine);//Debugging purposes only
                                            looper.Set(run, fracturedLine[1], arg[2], fracturedLine[3]); //Sends the needed values to the loop class
                                            Console.WriteLine("arg[0] = " + arg[0] + " arg[1] = " + arg[1] + " arg[2] = " + arg[2]);//Debugging purposes only
                                            looper.ExecuteLoop(MainForm, var, this, op, loopAtLine, run); //calls the 'ExecuteLoop' method, which will execute the command as the user intended
                                        }
                                        else
                                        {
                                            if (int.TryParse(fracturedLine[1], out int temp))//If in syntax-checking mode, throw an exception if the first parameter is an integer
                                            {
                                                throw new Exception("First value must be a variable (Infinite Loop Prevention)");
                                            }
                                        }
                                    }
                                    else //Throws if the user tries to use an '=' inside the while loop's conditions
                                    {
                                        throw new Exception("Invalid operator: \"" + arg[2] + "\" used in while loop");
                                    }
                                }
                                else //Throws if the line doesn't match the format for a while loop
                                {
                                    throw new Exception("Invalid command format, use:\n while([val/var] [operation] [val/var])");
                                }
                            }
                            else //Throws if there are too few or too many arguments for a while loop
                            {
                                throw new Exception("Invalid number of parameters: Ensure arguments are seperated with whitespace");
                            }

                            break;
                        case "endwhile": //Again, just serves as a marker for the 'while' loop, this just defines it as a valid command
                            Console.WriteLine("Reached 'Endwhile'");//Debugging purposes only
                            break;
                        case "print": //prints a given output unless in syntax mode
                            if (run)
                            {
                                myCanvas.FormatOutput(arg[1], MainForm);

                            }
                            break;
                        case "circle":

                            if (args != 2)//Validates the value of 'args'
                            {
                                throw new System.ArgumentException("ERROR: Invalid number of arguments");//Exception thrown for the surrounding try/catch to deal with
                            }
                            int argToInt = Int32.Parse(arg[1]);
                            if (run)
                            {
                                Console.WriteLine("circle");//Ignore - Debugging Purposes Only
                                myCanvas.DrawCircle(argToInt, MainForm.fill);/* Calls the DrawCircle method, parsing 
                                                                              * and passing the 'arg[1]' variable, 
                                                                              * and the fill variable from 'MainForm'
                                                                              */
                            }
                            break;
                        case "pen":
                            if (args != 2)//Validates the value of 'args'
                            {
                                throw new System.ArgumentException("ERROR: Invalid number of arguments");//Exception thrown for the surrounding try/catch to deal with
                            }
                            if (run)
                            {
                                myCanvas.SetColour(arg[1], this.MainForm);/*Calls the SetColour method, parsing 
                                                                 * and passing the 'arg[1]' variable, 
                                                                 * and the form 'MainForm'
                                                                 */
                            }
                            break;
                        case "setsize":
                            if (args != 2)//Validates the value of 'args'
                            {
                                throw new System.ArgumentException("ERROR: Invalid number of arguments");//Exception thrown for the surrounding try/catch to deal with
                            }
                            if (run)
                            {
                                myCanvas.SetSize(arg[1]);/*Calls the SetSize method, parsing 
                                                * and passing the 'arg[1]' variable 
                                                */
                            }
                            break;
                        case "fill":
                            if (args != 2)//Validates the value of 'args'
                            {
                                throw new System.ArgumentException("ERROR: Invalid number of arguments");//Exception thrown for the surrounding try/catch to deal with
                            }
                            if (run)
                            {
                                Console.WriteLine("fill");//Ignore - Debugging Purposes Only
                                myCanvas.SwitchFill(arg[1], MainForm);/*Calls the SwitchFill method,  
                                                             * passing the 'arg[1]' variable
                                                             * and the form 'MainForm'
                                                             */
                            }
                            break;
                        case "square":
                            if (args != 2)//Validates the value of 'args'
                            {
                                throw new System.ArgumentException("ERROR: Invalid number of arguments");//Exception thrown for the surrounding try/catch to deal with
                            }
                            if (run)
                            {
                                Console.WriteLine("square " + MainForm.fill);//Ignore - Debugging Purposes Only
                                myCanvas.DrawSquare(Int32.Parse(arg[1]), MainForm.fill);/*Calls the DrawSquare method,  
                                                                               * passing the 'arg[1]' variable
                                                                               * and the fill variable from 'MainForm'
                                                                               */
                            }
                            break;
                        case "drawto":
                            if (args != 3)//Validates the value of 'args'
                            {
                                throw new System.ArgumentException("ERROR: Invalid number of arguments");//Exception thrown for the surrounding try/catch to deal with
                            }
                            if (run)
                            {
                                Console.WriteLine("drawto");//Ignore - Debugging Purposes Only
                                myCanvas.DrawLine(Int32.Parse(arg[1]), Int32.Parse(arg[1]));
                                //Above - Calls the DrawLine method, parsing, and then passing the 'arg[1]' and 'arg[1]' variables 
                            }
                            break;
                        case "moveto":
                            if (args != 3)//Validates the value of 'args'
                            {
                                throw new System.ArgumentException("ERROR: Invalid number of arguments");//Exception thrown for the surrounding try/catch to deal with
                            }
                            Console.WriteLine("moveTo");//Ignore - Debugging Purposes Only
                            myCanvas.moveTo(Int32.Parse(arg[1]), Int32.Parse(arg[1]));
                            //Above - Calls the MoveTo method, parsing, and then passing the 'arg[1]' and 'arg[1]' variables 

                            break;
                        case "rect":
                            if (args != 3)//Validates the value of 'args'
                            {
                                throw new System.ArgumentException("ERROR: Invalid number of arguments");//Exception thrown for the surrounding try/catch to deal with
                            }
                            if (run)
                            {
                                Console.WriteLine("rectangle");//Ignore - Debugging Purposes Only
                                myCanvas.DrawRectangle(Int32.Parse(arg[1]), Int32.Parse(arg[1]), MainForm.fill);
                                //Above - Calls the DrawLine method, parsing, and then passing the 'arg[1]' and 'arg[1]' variables 
                            }
                            break;
                        case "triangle":
                            if (args != 3)//Validates the value of 'args'
                            {
                                throw new System.ArgumentException("ERROR: Invalid number of arguments");//Exception thrown for the surrounding try/catch to deal with
                            }
                            if (run)
                            {
                                Console.WriteLine("triangle");//Ignore - Debugging Purposes Only
                                myCanvas.DrawEqualTriangle(Int32.Parse(arg[1]), Int32.Parse(arg[1]), MainForm.fill);
                                /*
                                 * Above - Calls the DrawEqualTriangle method, parsing, and then passing the 'arg[1]' and 'arg[1]' variables, and
                                 * passing the fill variable from 'MainForm'
                                 */
                            }
                            break;
                        default://If no cases were ran, then the command is invalid, so an exception is thrown to be caught by the surrounding try
                            Console.WriteLine("Default case");//Ignore - Debugging Purposes Only
                            throw new System.Exception("ERROR: Unrecognised command");//Throws a generic exception
                    }
                }
            }
            catch (FormatException)//A format exception will be caused by an invalid parameter
            {
                myCanvas.FormatOutput("ERROR: Invalid parameter - Variable missing definition (line " + counter + ")", MainForm);
                //The 'FormatOutput' method is called and a desired message is passed to be outputted to the user
                //The 'counter' and 'MainForm' variables are also passed as they're needed for the method to function properly
                errorCount += 1;
            }
            catch (ArgumentException ex)//A format exception will be caused by an incorrect number of arguments being in 'fracturedLine'
            {
                Console.WriteLine("Format Exception Catch Hit");
                myCanvas.FormatOutput(ex.Message + " (line " + counter + ")", MainForm);
                //The 'FormatOutput' method is called and a message is passed to be outputted to the user
                errorCount += 1;
            }
            catch (Exception ex)//A generic Exception is passed when an invalid command is passed to this method
            {
                Console.WriteLine("Exception Catch Hit");
                myCanvas.FormatOutput(ex.Message + " (line " + counter + ")", MainForm);
                //The 'FormatOutput' method is called and a desired message is passed to be outputted to the user
                errorCount += 1;
            }

        }
        /// <summary>
        /// This method allows for various non-graphical commands to be executed in testing
        /// </summary>
        /// <param name="run">whether or not the method is being called in syntax-testing mode</param>
        /// <param name="counter">The line number of the command</param>
        /// <param name="arg">The arguments of the command</param>
        private void executeNonGraphicalCommands(int counter, bool run = true, params string[] arg)
        {
            /*
             *This method is exactly the same in function, and action as the executeCommand() method,
             *the difference is that this method contains no reference to graphics or the 'MainForm' form, and was seemingly 
             *the only way in which unit tests could be properly ran
             */
            Console.WriteLine("Run state is: " + run);
            Console.WriteLine("CURRENT LINE: " + line);
            Console.WriteLine("Current Match: " + op.Matches(line));

            //This set of 'if' and 'elseif' statments checks against all available commands to see if any match what the user has typed
            /*
             * If the input can be determined from one specific string/argument, e.g. a 'print' command, 
             * it is validated inside the switch statement further down.  
             * 
             * If it requires more arguments to validate, or a less specific one e.g. a method call, 
             * it is validated in one of the 'if/elseif' statements below.
             */
            if (op.Matches(line).Equals("Method_Call") == true) //If the inputted line matches the regex for a method call... 
            {

                Console.WriteLine("Recognised Method Call. Command: " + arg[0]); //Ignore - Debugging Purposes Only

            }
            else if (op.Matches(line).Equals("Op_With_Assignment") == true)//If the inputted line matches the regex for a operation with an assigmnent..
                                                                           // e.g. Variable = Variable +  1
            {

                op.MultStageOp(fracturedLine[0], run, fracturedLineValues); //Execute the method from the 'operation' class

            }
            else if ((op.Matches(line).Equals("Declaration") == true) & (op.Matches(line).Equals("While_Loop") == false) & (op.Matches(line).Equals("IF_Statement") == false))
            //If the line matches the regex for a declaration, and doesn't match that of a 'while' loop or 'if' statement...
            {
                if (arg[1].Equals("=")) //If the second value in the command string is an '=' sign...
                {
                    Console.WriteLine("Assigning variable...I think");//Debugging purposes
                    var.Set(run, fracturedLine[0], arg[2]);//Call the 'Set' method from the 'Variable' class
                }
            }
            else if (line.Equals("")) //Covers the user putting blank lines, prevents it resulting in an user-side exception output
            {
            }
            else
            {
                switch (arg[0])//Each command have a switch case, if there isn't a case for it, it's an invalid command
                {
                    
                    case "def":
                        if (op.Matches(line).Equals("Method_Declaration") == true) //If the format isn't right for a method definition, throw an exception
                        {

                            Console.WriteLine("Recognised Method Declaration. Command: " + arg[1]);//Debuggin purposes only
                           
                        }
                        else
                        {
                            throw new Exception("Invalid format for method definition, use: def [Method_Name]()");
                        }
                        break;
                    case "if":
                        Exception exceptIF = new Exception("Invalid command format, use: if([val/var] [operation] [val/var])"); //Defines a new exception to be thrown
                        if (op.Matches(line).Equals("IF_Statement")) //If the line doesn't match the format for an 'if' statement, throw the previously defined exception
                        {
                            if (arg.Length == 4) //The format can be matched without the correct number of arguments (whitespace usually does this)
                            {                    //So check the numeber of arguments to avoid errors
                                Console.WriteLine("Command recognised as an IF statement");//Debugging purposes only

                            }
                            else //If there are less or more than four arguments, throw an exception
                            {
                                throw exceptIF;
                            }
                        }
                        else //If the line doesn't match the format for an 'IF' statement, throw an exception
                        {
                            throw exceptIF;
                        }
                        break;
                    case "endif": //Ensures the program doesn't mistake 'endif' for an invalid command, even though it only serves as marker for the 'if' statment
                        break;
                    case "clear": //Will do nothing if in syntax mode and will clear the screen otherwise
                        Console.WriteLine("clear");//Ignore - Debugging Purposes Only
                        if (run)
                        {
                            myCanvas.ClearScreen();//Calls the method in canvas to perform the command
                        }
                        break;
                    case "center": //Will do nothing if in syntax mode and will execute a method otherwise
                        if (run)
                        {
                            myCanvas.Center();//Calls the method in canvas to perform the command
                        }
                        break;
                    case "reset":
                        if (run) //Will do nothing if in syntax mode and will execute a method otherwise
                        {
                            Console.WriteLine("reset");//Ignore - Debugging Purposes Only
                            myCanvas.ResetPos();//Calls the method in canvas to perform the command
                        }
                        break;
                    case "enddef"://Will do nothing if in syntax mode and will execute a method otherwise
                        break;
                    case "while": /*
                                       * Sets the parameters of the command, like start point, and the operation to peform,
                                       * as well as the values to perform it on
                                       * 
                                       * It then calls the 'ExecuteLoop' method from the 'looper' class, running the 'while' loop
                                       */

                        if (arg.Length == 4) //Makes sure the correct number of arguments are present, throws an exception if not
                        {
                            if (op.Matches(line).Equals("While_Loop")) //Checks the format of the line is correct, again, throwing an exception if not
                            {
                                if (op.CheckOp(arg[2]) == true & (arg[2].Equals("=") == false)) //<ales sure the user doesn't try an assigment inside the loop definition
                                {
                                    if (run) //If in syntax mode, don't run any functional code, otherwise, run it all
                                    {
                                        Console.WriteLine("Loop match successful");//Debugging purposes only
                                        loopAtLine = (counter - 1); //Sets the tag to let the loop know where to start looping
                                        Console.WriteLine("Loop tag found at line: " + loopAtLine);//Debugging purposes only
                                        looper.Set(run, fracturedLine[1], arg[2], fracturedLine[3]); //Sends the needed values to the loop class                                      
                                    }
                                    else
                                    {
                                        if (int.TryParse(fracturedLine[1], out int temp))//If in syntax-checking mode, throw an exception if the first parameter is an integer
                                        {
                                            throw new Exception("First value must be a variable (Infinite Loop Prevention)");
                                        }
                                    }
                                }
                                else //Throws if the user tries to use an '=' inside the while loop's conditions
                                {
                                    throw new Exception("Invalid operator: \"" + arg[2] + "\" used in while loop");
                                }
                            }
                            else //Throws if the line doesn't match the format for a while loop
                            {
                                throw new Exception("Invalid command format, use:\n while([val/var] [operation] [val/var])");
                            }
                        }
                        else //Throws if there are too few or too many arguments for a while loop
                        {
                            throw new Exception("Invalid number of parameters: Ensure arguments are seperated with whitespace");
                        }

                        break;
                    case "endwhile": //Again, just serves as a marker for the 'while' loop, this just defines it as a valid command
                        Console.WriteLine("Reached 'Endwhile'");//Debugging purposes only
                        break;
                    case "moveto":
                        if (args != 3)//Validates the value of 'args'
                        {
                            throw new System.ArgumentException("ERROR: Invalid number of arguments");//Exception thrown for the surrounding try/catch to deal with
                        }
                        Console.WriteLine("moveTo");//Ignore - Debugging Purposes Only
                        myCanvas.moveTo(Int32.Parse(arg[1]), Int32.Parse(arg[1]));
                        //Above - Calls the MoveTo method, parsing, and then passing the 'arg[1]' and 'arg[1]' variables 
                        break;
                    default:
                        Console.WriteLine("Default case");//Ignore - Debugging Purposes Only
                        throw new System.Exception("Unrecognised command");//Exception thrown for the test case to deal with
                }
            }
        }
    }
}
