﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Contains the methods to check if a given line contains a method call
    /// </summary>
    class MethodCallHandler : AbstractHandler
    {
        /// <summary>
        /// Overrides the 'Handle' method, checks to see if the given input contains a method call
        /// </summary>
        /// <param name="request">The line to be checked</param>
        /// <param name="op">Instance of the 'Operation' class</param>
        /// <param name="var">Instance of the 'Variable' class</param>
        /// <param name="p">Instance of the 'Parser' class</param>
        /// <param name="method">Instance of the 'Method' class</param>
        /// <param name="args">The arguments of the command</param>
        /// <param name="run">Whether or not the program is in syntax-checking mode</param>
        /// <param name="fracturedLine">The raw line input</param>
        /// <param name="mainForm">Instance of the 'MainForm' form</param>
        /// <param name="fracturedLineValues">The values of any variables contained in the input</param>
        /// <returns>Either returns an informative string, or the base handler method - used to 'handle' the next object</returns>
        public override object Handle(object request, Operation op, Variable var, Parser p, Methods method, string[] args, bool run, string[] fracturedLine, MainForm mainForm, string[] fracturedLineValues)
        {
            if (op.Matches(request as string).Equals("Method_Call") == true)//If the inputted line matches the regex for a operation with an assigmnent..
            {
                method.ExecuteMethod(mainForm, p, run, args[0]); //execute the method using the given parameters
                return "Method: Recognised"; //Return an informative string
            }
            else
            {
                //If the regex doesn't match, then call the 'Handle' method from the 'AbstractHandler' class - moves on to the next handler object
                return base.Handle(request, op, var, p, method, args, run, fracturedLine, mainForm, fracturedLineValues);
            }
        }
    }
}
