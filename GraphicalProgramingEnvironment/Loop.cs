﻿using System;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Sets up the methods required to execute a looping command in the program - inherits methods from the 'Command' class
    /// - By Joshua Hooper: 33532281
    /// </summary>
    public class Loop : Command
    {
        bool repeats;
        string variableValue_1;
        string variableValue_2;
        string operandValue;
        string variableValue_1_Name;
        string variableValue_2_Name;
        //Setting up variables to contain objects of the 'Operation' and 'Variable' classes
        Operation op; 
        Variable var;
        /// <summary>
        /// Sets default values for a new loop object
        /// </summary>
        public Loop() //Setting up a default constructor for the class
        {
            repeats = true;
        }
        /// <summary>
        /// Provides the functionality for the user to loop between two points a given number of times
        /// </summary>
        /// <param name="main">The form from which the commands  are being run</param>
        /// /// <param name="var">Referencing an object of the 'Variable' class</param>
        /// <param name="p">The parser that will be running the looped commands</param>
        /// <param name="start">The line on which the loop begins (user-defined)</param>
        /// /// <param name="op">Referencing an object of the 'Operation' class</param>
        /// <param name="run">Whether or not the program is in syntax-checking mode</param>
        public void ExecuteLoop(MainForm main, Variable var, Parser p, Operation op, int start, bool run = true)
        {

            this.var = var;
            this.op = op;
            int endLine = FindStr(main, "endwhile");  //Attempts to find the string 'endif' inside the textbox's text
            Console.WriteLine("Starting on line: [" + start + "]");//Debugging purposes only
            Console.WriteLine("Ending on line: [" + endLine + "]");//Debugging purposes only
            if (endLine != -1) //As long as 'FindStr' doesn't store '-1' in 'endLine', run the rest of the code 
            {
                Console.WriteLine("Run in loop is: " + run);
                if (run)
                {
                    String[] lines = main.activeWindow.Lines; //Takes the lines from the textbox, and stores them in this array
                    //Loops from the start, to the end point

                    while (repeats == true) //Repeats is false if the condition set by the user is no longer true
                    {
                        Console.WriteLine("Looping...");//Debugging purposes only
                        for (int counter = start + 1; counter < endLine; counter++)
                        {
                            Console.WriteLine("line: " + counter);//Debugging purposes only
                            p.ParseCommand(lines[counter].ToLower().Trim(), lineCounter: counter, looping: true); //Parses each line - just like the method in 'MainForm'
                        }
                        if (CheckStopCondition() == false) //Calls, and checks the return of the 'CheckStopCondition' method to see if looping needs to continue
                        {
                            Console.WriteLine("Stop Condition Reached");//Debugging purposes only
                            this.repeats = false; //Sets the 'repeats' variable to 'false', which will cause the looping to cease
                        }
                    }
                    //The below two lines make the parser skip through the given lines until at a point past the loop
                    p.skipLines = true; 
                    p.skipTo = endLine;
                    p.counter = start;
                    Console.WriteLine($"Loop finished executing, jumping to endwhile flag ({endLine}) currentline = {p.counter} linecount = {p.lineCount}");//Debugging purposes only
                }
            }
            else //If no enclosing 'endwhile' exists, the loop will not work correctly, so throw an exception to inform the user
            {
                throw new Exception("ERROR: No enclosing 'endwhile' found"); 
            }
        }
        /// <summary>
        /// Inherited method from 'Command' class - Serves no purpose but must still be defined
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public override string Get(params string[] args) //As this class inherits methods from the command class, this method must be implemented, but serves no purpse
        {
            throw new NotImplementedException(); //This method is not called at any point, but just in case, if it is called, it will throw an exception
        }
        /// <summary>
        /// Checks if the stop condition - set out at the start of the loop, has been met
        /// </summary>
        /// <param name="run">Checks if the method has been called in syntax-checking mode</param>
        /// <returns></returns>
        public bool CheckStopCondition(bool run = true) //Checks if the stop condition, set by the user, is false
        {
            variableValue_1 = var.Get(variableValue_1_Name); 
            if(variableValue_1.Equals("false")) //To prevent the user making program-crashing infinite loops, a loop requires the first value to be a variable
            {
                throw new Exception("Runtime Error: First value must be a valid variable"); //If the first value isn't a valid variable, an exception is thrown
            }
            if (var.Get(variableValue_2_Name).Equals("false"))//Treats the given value as a variable first, and as a value if no variable is found
            {//If its value is false, then no variable exists for it, so set its value as the original input value
                variableValue_2 = variableValue_2_Name;
            }
            else
            {
                variableValue_2 = var.Get(variableValue_2_Name); //If a value is returned, assign this to the 'variableValue_2' variable
            }
            Console.WriteLine("CheckStopCondition: variable val1 =" + variableValue_1);//Debugging purposes only
            Console.WriteLine("CheckStopCondition: variable val2 =" + variableValue_2);//Debugging purposes only
            Console.WriteLine("Has stop condition been reached? " + op.PerformOp(run, operandValue, variableValue_1, variableValue_2));//Debugging purposes only
            return bool.Parse(op.PerformOp(run, operandValue, variableValue_1, variableValue_2)); 
            //Calls the 'performOp' method to return a value after passing two values and an operation to perform on them
        }

        /// <summary>
        ///  Allows the user to set the start and end points, as well as the number of times that the loop will repeat
        /// </summary>
        /// <param name="run">Whether or not the program is running in syntax mode</param>
        /// <param name="list">The arguments to be set</param>
        public override void Set(bool run = true, params string[] list)
        {
            //Assigns the given parameters to their relevant variable in the class
            variableValue_1_Name = list[0]; 
            variableValue_2_Name = list[2];
            operandValue = list[1];
        }
    }
}
