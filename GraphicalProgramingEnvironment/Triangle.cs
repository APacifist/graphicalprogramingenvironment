﻿using System.Drawing;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Handles manipulating and drawing a triangle, inherits properties from the 'Shape' class
    /// </summary>
    public class Triangle : Shape
    {
        /// <summary>
        /// The width and height of the created triangle
        /// </summary>
        public int width, height; //Creates the 'width' and 'height' variables for use in methods throughout this class
        /// <summary>
        /// Handles setting up a 'Triangle' with a default 'width' and 'height' of 20, also is passed the 'testingMode' boolean
        /// </summary>
        /// <param name="testingMode">Whether or not testing mode is active</param>
        public Triangle(bool testingMode) : base(testingMode)
        {
            //Setting the width and height to be 20 by default
            width = 20;
            height = 20;
        }
        /// <summary>
        /// Sets up a Triangle with the various properties needed to draw it, along with the testingMode boolean for testing purposes
        /// </summary>
        /// <param name="pen">The pen that will be used to draw the triangle</param>
        /// <param name="x">The 'x' value of the triangle</param>
        /// <param name="y">The 'y' value of the triangle</param>
        /// <param name="width">The width of the triangle</param>
        /// <param name="height">The height of the triangle</param>
        /// <param name="testingMode">Whether or not testing mode is active</param>
        public Triangle(Pen pen, int x, int y, int width, int height, bool testingMode) : base(pen, x, y, testingMode)
        {
            //Setting the width and the height to that passed in this constructor
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// Overrides the 'Set' method from 'Shape' to add the ability to set the width and height of the triangle
        /// </summary>
        /// <param name="pen">The pen that will be used to draw the triangle</param>
        /// <param name="list">The variable holding the various integers such as the width, height and the x and y positions</param>
        public override void set(Pen pen, params int[] list)
        {
            base.set(pen, list[0], list[1]); //Using the 'set' method from shape to set the pen, x and y values
            this.width = list[2]; //setting the width to the second (technically third) value in list[]
            this.height = list[3]; //setting the height to the third (technically fourth) value in list[]
        }
        /// <summary>
        /// Overrides the 'draw' method from 'Shape' to add functionality
        /// Takes a 'width' and 'height' value, and uses five points that together form a triangle
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g)
        {
            
            /*Why it's five points:
             * Starts in the middle of base line, goes from there, so 3 points in triangle's base 
             * and final point is the same as the starting point (has to be in order to be a complete shape), so 4 point in triangle's base 
             * so 4 points in shape's base, plus top middle point (point of the triangle) = five total points.
             */
            int tempX = this.x; //assigns the 'x' value to 'tempX' so it's value can be freely manipulated without affecting the rest of the code
            int tempY = this.y;//assigns the 'y' value to 'tempY' so it's value can be freely manipulated without affecting the rest of the code
            int tempWidth = (width / 2); //Used to center the triangle around the pen position
            int tempHeight = (height / 2);//Used to center the triangle around the pen position

            Point[] trianglePoints = new Point[5]; //Creates a variable to store all the upcoming points

            trianglePoints[0] = new Point(tempX, tempY += tempHeight);   //Bottom-Middle triangle point
            trianglePoints[1] = new Point(tempX += tempWidth, tempY);   //Bottom-Right triangle point
            trianglePoints[2] = new Point(tempX -= tempWidth, tempY -= height);//Top-Middle triangle point
            trianglePoints[3] = new Point(tempX -= tempWidth, tempY += height);//Bottom-Left triangle point
            trianglePoints[4] = new Point(tempX += tempWidth, tempY);//Bottom-Middle triangle point (again)
            g.DrawPolygon(pen, trianglePoints); //Draws a polygon (a triangle in this case) using the points set previously
        }
        /// <summary>
        /// Overrides the 'drawFilled' method from 'Shape' to add functionality
        /// Takes a 'width' and 'height' value, and uses five points that together form a filled triangle
        /// </summary>
        /// <param name="g"></param>
        public override void drawFilled(Graphics g)
        {
            SolidBrush solidB = new SolidBrush(pen.Color); //Creates a solid brush - used to draw filled shapes
            int tempX = this.x; //assigns the 'x' value to 'tempX' so it's value can be freely manipulated without affecting the rest of the code
            int tempY = this.y; //assigns the 'y' value to 'tempY' so it's value can be freely manipulated without affecting the rest of the code
            int tempWidth = (width / 2); //Used to center the filled triangle around the pen position
            int tempHeight = (height / 2); //Used to center the filled triangle around the pen position

            Point[] trianglePoints = new Point[5];//Creates a variable to store all the upcoming points

            trianglePoints[0] = new Point(tempX, tempY += tempHeight); //Bottom-Middle triangle point
            trianglePoints[1] = new Point(tempX += tempWidth, tempY); //Bottom-Right triangle point
            trianglePoints[2] = new Point(tempX -= tempWidth, tempY -= height); //Top-Middle triangle point
            trianglePoints[3] = new Point(tempX -= tempWidth, tempY += height); //Bottom-Left triangle point
            trianglePoints[4] = new Point(tempX += tempWidth, tempY); //Bottom-Middle triangle point (again)
            g.FillPolygon(solidB, trianglePoints);//Draws a filled polygon (a filled triangle in this case) using the points set previously
        }
    }
}
