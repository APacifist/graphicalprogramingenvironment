﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Handles manipulating and drawing a circle, inherits properties from the 'Shape' class
    /// </summary>
    class Circle : Shape
    {
        int radius; //Creating the 'radius' variable here so it may be used from any method in the class
        /// <summary>
        /// Constructor for creating a default circle, takes 'testingMode' as a parameter.
        /// Assigns a value of '10' to radius
        /// </summary>
        /// <param name="testingMode"></param>
        public Circle(bool testingMode) : base(testingMode) //takes 'testingMode' for use when calling this class during testing
        {
            radius = 10;//Sets the default 'radius' to 10
        }
        /// <summary>
        /// Sets up circle with the various properties needed to draw it, along with 'testingMode' for testing purposes.
        /// Inherits properties from 'Shape
        /// </summary>
        /// <param name="pen">The pen that will be used to draw the circle</param>
        /// <param name="x">The 'x' value of the circle</param>
        /// <param name="y">The 'y' value of the circle</param>
        /// <param name="radius">The radius of the circle</param>
        /// <param name="testingMode">Whether or not testing mode is active</param>
        public Circle(Pen pen, int x, int y, int radius, bool testingMode) : base(pen, x, y, testingMode)//takes 'testingMode' for use when calling this class during testing
        {
            this.radius = radius; //Sets the class' value of 'radius' equal to the value of the parameter 'radius'
        }
        /// <summary>
        /// Overrides the 'Set' method from 'Shape' to add the ability to set the radius of the circle
        /// </summary>
        /// <param name="pen">The pen that will be used to draw the shape</param>
        /// <param name="list">The variable holding the various integers such as the radius, and the x and y position</param>
        public override void set(Pen pen, params int[] list) //Overrides the set method to allow for both the Pen, and the radius to be assigned
        {
            base.set(pen, list[0], list[1]); //calls the set method from the base (Shape) class
            this.radius = list[2]; //sets the radius to be equal to the value of the parameter 'radius' passed here
        }
        /// <summary>
        /// Overrides the draw method to add functionality to it. 
        /// Couldn't be done in the 'Shape' Class as it was a generic class
        /// </summary>
        /// <param name="g">Graphics context</param>
        public override void draw(Graphics g)
        {
            g.DrawEllipse(pen, x-radius, y-radius, radius * 2, radius * 2); //Calls the 'DrawElipse' method with the x,y and radius values
            // (x-radius & y-radius done to center the circle around the x and y coordinates when drawn)
        }
        /// <summary>
        /// Overrides the drawFilled method to add functionality to it. 
        /// Couldn't be done in 'Shape' as it was a generic class
        /// </summary>
        /// <param name="g">Graphics context</param>
        public override void drawFilled(Graphics g) 
        {
            SolidBrush solidB = new SolidBrush(pen.Color); //Defines a solid brush - used to draw filled shapes
            g.FillEllipse(solidB, x - radius, y - radius, radius * 2, radius * 2); 
            //Draws the filled circle using the solid brush, the radius, x and y variables
        }
    }
}
