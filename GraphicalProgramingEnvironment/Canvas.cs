﻿using System;
using System.Drawing;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// The 'Canvas' class contains the commands needed to manipulate the graphics in the bitmap, 
    /// as well as some other functional commands
    /// - by Joshua Hooper: 33532281
    /// </summary>
    public class Canvas
    {
        //Declaring the variables needed inside this class
        bool testingMode;
        float penWidth;
        Graphics g;
        Pen Pen;
        /// <summary>
        /// the factory class - used to retrieve commands and shapes
        /// </summary>
        Factory f;
        /// <summary>
        /// The 'x' and 'y' positions of the pen
        /// </summary>
        public int xPos, yPos;

        /// <summary>
        /// Constructor for the 'Canvas' class that sets up the class for use in a testing environment. 
        /// This is neccessary as the testing environment doesn't contain any graphics capabilities, so these must be 
        /// avoided when in testing mode.
        /// </summary>
        /// <param name="testingMode">Informs as to whether or not the class is being instantiated in a testing environment</param>
        public Canvas(bool testingMode = true)
        {
            this.testingMode = testingMode; //Assigns the parameter's value to the testingMode variable, so it can be used throughout the class
            xPos = yPos = 0; //Sets the xPos and yPos variables to their starting position (0,0)
        }
        /// <summary>
        /// Constructor for the 'Canvas' class that sets up the class for normal use (not a testing environment).
        /// This involves creating a new pen object, as well as assigning variables to parameters such as width. It also
        /// involves assigning the graphics context passed in the constructor to that of the variable 'g' 
        /// inside the class - allows it to be used throughout the various methods inside this class.
        /// </summary>
        /// <param name="g">The graphics context, neccessary for all graphics-based commands</param>
        public Canvas(Graphics g = default)
        {
            //Assigning values to the neccessary variables, and setting up the required objects
            penWidth = 5;
            this.g = g;
            xPos = yPos = 0;
            Pen = new Pen(Color.Black, penWidth); //Instantiates a new pen object and assigns it to the 'Pen' variable so it may be used and manipulated within the class
            f = new Factory(); //Creating an instance of the 'Factory' object
        }

        /// <summary>
        /// Faciliates the updating of the 'xPos' and 'yPos' variables to a new set of values defined by the method's
        /// arguments.
        /// Calls the 'checkLimits' method which ensures the given values reside within a valid range.
        /// </summary>
        /// <param name="toX">The desired 'X' value to be moved to</param>
        /// <param name="toY">The desired 'Y' value to be moved to</param>
        public void moveTo(int toX, int toY)
        {
            checkLimits(toX, toY);
            (this.xPos, this.yPos) = (toX, toY);
            Console.WriteLine(this.xPos + " " + this.yPos); //Ignore - Debugging purposes only
        }
        /// <summary>
        /// Calls the drawline command on the graphics passed in the 'g' variable, draws a line from the current 
        /// 'xPos' and 'yPos' value, to the 'toX' and 'toY' values given as arguments, and updates the 'Xpos' and
        /// 'yPos' values to reflect the pen's new position.
        /// Calls the 'checkLimits' method which ensures the given values reside within a valid range.
        /// </summary>
        /// <param name="toX">The desired 'X' value to be drawn to</param>
        /// <param name="toY">The desired 'Y' value to be drawn to</param>
        public void DrawLine(int toX, int toY)
        {
            checkLimits(toX, toY);
            if (!testingMode)//Stops graphical commands runing whilst in testing mode
            {
                g.DrawLine(Pen, xPos, yPos, toX, toY); //draws a line from 'xPos','yPos' to 'toX','toY' using a pen defined by the 'Pen' variable
            }
            xPos = toX;
            yPos = toY;
        }
        /// <summary>
        /// Creates a new 'Square' object and uses this to graphically draw a square with the size, and whether or not it is filled,
        /// specified in the method's arguments.
        /// </summary>
        /// <param name="size"> The desired width and height - can use one value for both by virtue of the shape being a square</param>
        /// <param name="fill">Whether or not the shape is to be filled, or just an outline of the shape</param>
        public void DrawSquare(int size, bool fill)
        {
            if (!testingMode)
            {
                Shape sqr = f.GetShape("square", testingMode);
                sqr.set(Pen, xPos, yPos, size, size); //Creating a new 'Square' object#
                if (fill)//Calls the drawFilled method of the square object if 'fill' is true
                {
                    sqr.drawFilled(g);
                }
                else//Calls the regular draw method of the square object otherwise
                {
                    sqr.draw(g);
                }
            }
        }
        /// <summary>
        /// Creates a new 'Rectangle' object and uses this to graphically draw a rectangle with the width, height, and whether or not it is filled
        /// specified in the method's arguments.
        /// </summary>
        /// <param name="height">The height of the shape</param>
        /// <param name="width">The width of the shape</param>
        /// <param name="fill">Whether or not the shape is to be filled, or just an outline of the shape</param>
        public void DrawRectangle(int height, int width, bool fill)
        {
            if (!testingMode)
            {
                Shape rect = f.GetShape("rectangle", testingMode);
                rect.set(Pen, xPos, yPos, width, height);//Creating a new 'Rectangle' object

                if (fill)
                {
                    Console.WriteLine("drawing filled rectangle");//Ignore - Debugging purposes only
                    rect.drawFilled(g);//Calls the drawFilled method of the 'Rectangle' object if 'fill' is true
                }
                else
                {
                    rect.draw(g);//Calls the regular draw method of the 'Rectangle' object otherwise
                }
            }

        }
        /// <summary>
        /// Creates a new 'Triangle' object and uses this to graphically draw a triangle with the width, height, and whether or not it is filled
        /// specified in the method's arguments.
        /// </summary>
        /// <param name="width">The width of the shape</param>
        /// <param name="height">The height of the shape</param>
        /// <param name="fill">Whether or not the shape is to be filled, or just an outline of the shape</param>
        public void DrawEqualTriangle(int width, int height, bool fill)
        {
            if (!testingMode)
            {
                Shape tri = f.GetShape("triangle", testingMode);
                tri.set(Pen, xPos, yPos, width, height);//Creating a new 'Triangle' object
                if (fill)
                {
                    Console.WriteLine("drawing filled triangle");//Ignore - Debugging purposes only
                    tri.drawFilled(g);//Calls the drawFilled method of the 'Triangle' object if 'fill' is true
                }
                else
                {
                    tri.draw(g);//Calls the regular draw method of the 'Triangle' object otherwise
                }
            }
        }
        /// <summary>
        /// Creates a new 'Circle' object and uses this to graphically draw a circle with the radius, and whether or not it is filled,
        /// specified in the method's arguments.
        /// </summary>
        /// <param name="radius">The radius of the circle</param>
        /// <param name="fill">Whether or not the shape is to be filled, or just an outline of the shape</param>
        public void DrawCircle(int radius, bool fill)
        {
            if (!testingMode)
            {
                Shape newCircle = f.GetShape("Circle", false); //Requesting a circle object from the factory
                newCircle.set(Pen, xPos, yPos, radius); //Setting up the new circle to have the properties specified by the user
                if (fill)//Calls the drawFilled method of the 'Circle' object if 'fill' is true
                {
                    Console.WriteLine("drawing filled circle");//Ignore - Debugging purposes only
                    newCircle.drawFilled(g);
                }
                else//Calls the regular draw method of the 'Circle' object otherwise
                {
                    newCircle.draw(g);
                }
            }
        }
        /// <summary>
        /// Facilitates setting the pen colour. This method also handles the provision of visual outputs to the user on
        /// the main form through displaying certain images when specific colours are selected.
        /// </summary>
        /// <param name="colour">A string containing a desired pen colour</param>
        /// <param name="main">The form on which the visual output is being displayed</param>
        public void SetColour(string colour, MainForm main)
        {
            /*
             * This switch statement handles ten colours, giving a visual output for each one,
             * if the input doesn't match any of these ten options, then the default statement triggers
             * which will search for the colour directly and produce a, more generic, visual output.         
             */

            /*
             * [!] NOTE [!] - The cases in this statement are very much alike, so going into detail on one such case 
             * will be sufficient to explain all cases (except the 'default' case)
             */
            switch (colour)
            {
                case "red":
                    /* 
                     * If the passed string is "red", then a new pen object is created and stored 
                     * within the 'Pen' variable. This new pen has its 'Color' property set to Color.Red and the 'Width'
                     * property set to the value of the 'penWidth' variable - this variable has a default value of 5 until 
                     * changed by the user by use of the 'setSize' method.
                    */
                    Console.WriteLine("Red");//Ignore - Debugging purposes only
                    if (!testingMode) //If testingMode is true, don't run any graphical commands
                    {
                        this.Pen = new Pen(Color.Red, penWidth);//Creates the new pen object with the desired colour.

                        allLightsOff(main); //Each colour has an assigned image, this method hides all assigned images for every colour.
                        main.pb_RedOn.Visible = true;//This makes the assigned image for this colour visible 
                    }
                    break;
                case "orange"://Creates a new pen object with the new colour, and presents a visual output the the user
                    Console.WriteLine("Orange");//Ignore - Debugging purposes only
                    if (!testingMode)
                    {
                        this.Pen = new Pen(Color.Orange, penWidth);

                        allLightsOff(main);
                        main.pb_OrangeOn.Visible = true;
                    }
                    break;
                case "yellow"://Creates a new pen object with the new colour, and presents a visual output the the user
                    Console.WriteLine("Yellow");//Ignore - Debugging purposes only
                    if (!testingMode)
                    {
                        this.Pen = new Pen(Color.Yellow, penWidth);

                        allLightsOff(main);
                        main.pb_YellowOn.Visible = true;
                    }
                    break;
                case "green"://Creates a new pen object with the new colour, and presents a visual output the the user
                    Console.WriteLine("Green");//Ignore - Debugging purposes only
                    if (!testingMode)
                    {
                        this.Pen = new Pen(Color.Green, penWidth);

                        allLightsOff(main);
                        main.pb_GreenOn.Visible = true;
                    }
                    break;
                case "blue"://Creates a new pen object with the new colour, and presents a visual output the the user
                    Console.WriteLine("Blue");//Ignore - Debugging purposes only
                    if (!testingMode)
                    {
                        this.Pen = new Pen(Color.Blue, penWidth);

                        allLightsOff(main);
                        main.pb_BlueOn.Visible = true;
                    }
                    break;
                case "pink"://Creates a new pen object with the new colour, and presents a visual output the the user
                    Console.WriteLine("Pink");//Ignore - Debugging purposes only
                    if (!testingMode)
                    {
                        this.Pen = new Pen(Color.Pink, penWidth);

                        allLightsOff(main);
                        main.pb_PinkOn.Visible = true;
                    }
                    break;
                case "purple"://Creates a new pen object with the new colour, and presents a visual output the the user
                    Console.WriteLine("Purple");//Ignore - Debugging purposes only
                    if (!testingMode)
                    {
                        this.Pen = new Pen(Color.Purple, penWidth);
                    
                    allLightsOff(main);
                    main.pb_PurpleOn.Visible = true;
            }
                    break;
                case "white"://Creates a new pen object with the new colour, and presents a visual output the the user
                    Console.WriteLine("White");//Ignore - Debugging purposes only
                    if (!testingMode)
                    {
                        this.Pen = new Pen(Color.White, penWidth);

                        allLightsOff(main);
                        main.pb_WhiteOn.Visible = true;
                    }
                    break;
                case "black"://Creates a new pen object with the new colour, and presents a visual output the the user
                    Console.WriteLine("Black");//Ignore - Debugging purposes only
                    if (!testingMode)
                    {
                        this.Pen = new Pen(Color.Black, penWidth);

                        allLightsOff(main);
                        main.pb_BlackOn.Visible = true;
                    }
                    break;
                case "brown"://Creates a new pen object with the new colour, and presents a visual output the the user
                    Console.WriteLine("Brown");//Ignore - Debugging purposes only
                    if (!testingMode)
                    {
                        this.Pen = new Pen(Color.Brown, penWidth);

                        allLightsOff(main);
                        main.pb_BrownOn.Visible = true;
                    }
                    break;
                default:
                    /*
                     * If no case was activated, then this case will trigger
                     * This case will format the input so it matches that of a valid 'Colour' -
                     * (capitalised first letter, the rest lowercase)
                     * and will attempt a similar command to those used in the cases above, this time taking the formatted
                     * string as the actual value for 'Color'
                     * As in the other cases, all other images will be hidden, and an image specific to this case will be shown
                     */
                    Console.WriteLine("Default Colour Case");//Ignore - Debugging purposes only
                    if (!testingMode)//Avoid graphical commands if testingMode is true
                    {
                        allLightsOff(main);//Hides all other colour-related images
                    main.pb_OtherOn.Visible = true; //Displays the image relevant to this case
                    
                   
                        this.Pen = new Pen(Color.FromName(char.ToUpper(colour[0]) + colour.Substring(1)), penWidth);
                        //Above - Formats the string so it can be directly passed as a colour for the new pen object
                    }
                    break;

            }

        }
        /// <summary>
        /// Allows the pen's brush size to be altered
        /// </summary>
        /// <param name="size">The new size of the pens brush</param>
        public void SetSize(string size)
        {
            /*
             * Only need to change the variable's value, as the new width value is
             * passed automatically whenever a drawing command is called
             */
            penWidth = float.Parse(size); //Sets the new width value
            Console.WriteLine(penWidth);//Ignore - Debugging purposes only
        }
        /// <summary>
        /// This method hides certain colour-related pictureboxes on the MainForm, this is a purely aesthetic functionality
        /// </summary>
        /// <param name="main">The form on which the visual output is being displayed</param>
        private void allLightsOff(MainForm main)
        {
            if (!testingMode) //If testingMode is true, don't run any graphical commands
            {
                //These commands set the 'Visible' property of the pictureboxes to 'false' 
                main.pb_BlueOn.Visible = false;
                main.pb_RedOn.Visible = false;
                main.pb_GreenOn.Visible = false;
                main.pb_YellowOn.Visible = false;
                main.pb_PinkOn.Visible = false;
                main.pb_OrangeOn.Visible = false;
                main.pb_PurpleOn.Visible = false;
                main.pb_WhiteOn.Visible = false;
                main.pb_BlackOn.Visible = false;
                main.pb_BrownOn.Visible = false;
                main.pb_OtherOn.Visible = false;
            }
        }
        /// <summary>
        /// This method clears all drawings from the graphics area using '.Clear()'
        /// </summary>
        public void ClearScreen()
        {
            if (!testingMode)
            {
                g.Clear(Color.White);
            }
        }
        /// <summary>
        /// This method sets the xPos and yPos values to 0 using the moveTo() method
        /// </summary>
        public void ResetPos()
        {
            moveTo(0, 0);
        }
        /// <summary>
        /// This method centers the xPos and yPos values so they are in the middle of the bitmap using the moveTo() method
        /// </summary>
        public void Center()
        {
            moveTo(293, 217);
        }
        /// <summary>
        /// This method takes a string input and appends the time to it before outputting the complete result into a textbox.
        /// </summary>
        /// <param name="text">The string to be appended to and outputted</param>
        /// <param name="main">The form in which the target textbox resides</param>
        public void FormatOutput(string text, MainForm main)
        {
            if (!testingMode)
            {
                main.outputWindow.AppendText("[" + DateTime.Now.ToString("h:mm:ss") + "] - " + text);
                main.outputWindow.AppendText(Environment.NewLine);
            }
        }
        /// <summary>
        /// This method checks that the values passed to it are within the bounds of the bitmap.
        /// If they are outside the bounds, it will throw an exception.
        /// </summary>
        /// <param name="toX">The desired 'X' value to be moved to</param>
        /// <param name="toY">The desired 'Y' value to be moved to</param>
        public void checkLimits(int toX, int toY)
        {
            //If both 'toX' and 'toY' are not within the bounds, throw an exception
            if (!((toX <= 585 & toX >= 0) & (toY <= 433 & toY >= 0)))
            {
                throw new System.FormatException("invalid parameters");
            }
        }
        /// <summary>
        /// This method sets the 'fill' variable - needed for filling shapes, to be either on or off
        /// </summary>
        /// <param name="input"></param>
        /// <param name="main"></param>
        public void SwitchFill(string input, MainForm main)
        {
            if (input.ToUpper().Equals("ON")) //If the input is ON, then set the variable to true and show the picturebox (aesthetic feature)
            {
                Console.WriteLine("Fill on");//Ignore - Debugging Purposes Only
                if (!testingMode) //If testingMode is true, don't run any graphical commands
                {
                    main.fill = true; //set the variable
                    main.pb_fillOn.Visible = true; //Aesthetic feature
                }
            }
            else if (input.ToUpper().Equals("OFF"))// If the input is OFF, then set the variable to false and hide the picturebox(aesthetic feature)
            {
                if (!testingMode) //If testingMode is true, don't run any graphical commands
                {
                    main.fill = false;//set the variable
                    main.pb_fillOn.Visible = false;//Aesthetic feature
                }

            }
            else//If anything other than 'ON' or 'OFF' is sent, throw an exception to be caught
            {
                throw new System.FormatException();
            }
        }
    }
}
