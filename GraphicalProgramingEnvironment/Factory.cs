﻿using System;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// The Factory Class provides functionality to be able to request the building of shapes, facilitates inversion of control across the program
    /// - By Joshua Hooper: 33532281
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// This method allows for shapes to be queried, should a shape be found, an object of it is returned
        /// </summary>
        /// <param name="shapeType">The string containing the name of the shape being queried</param>
        /// <param name="testingMode">Whether or not testing mode is active</param>
        /// <returns></returns>
        public Shape GetShape(String shapeType, bool testingMode)
        {
            shapeType = shapeType.ToLower().Trim();  //Formats the string input


            if (shapeType.Equals("circle")) //If the input is "circle", return a 'Circle' object
            {
                return new Circle(testingMode);
            }
            else if (shapeType.Equals("rectangle"))//If the input is "rectangle", return a 'Rectangle' object
            {
                return new Rectangle(testingMode);

            }
            else if (shapeType.Equals("triangle"))//If the input is "triangle", return a 'Triangle' object
            {
                return new Triangle(testingMode);

            }
            else if (shapeType.Equals("square"))//If the input is "square", return a 'Square' object
            {
                return new Square(testingMode);
            }
            else
            {
                //If the program reaches this point, the shape type is unknown, so an exception is thrown stating this
                System.ArgumentException argEx = new System.ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }

          
        }
        /// <summary>
        /// This method allows for commands to be queried, should a command be found, an object of it is returned
        /// </summary>
        /// <param name="commandType">The command string to be queried</param>
        /// <returns></returns>
        public Command GetCommand(String commandType)
        {
            commandType = commandType.ToLower().Trim();  //Formats the string input

            switch (commandType) 
            {

                case "loop":
                    // return new 'Loop' object
                    return new Loop();
                case "variable":
                    // return new 'Variable' object
                    return new Variable();
                case "if_statement":
                    // return new 'IfStatement' object
                    return new IfStatement();
                case "method":
                    // return new 'Methods' object
                    return new Methods();
               
                default:
                    Console.WriteLine("No command found"); //If no command is found using the given name, throw an exception
                    throw new System.ArgumentException("Unrecognised command");
            }   
        }

    }
}
