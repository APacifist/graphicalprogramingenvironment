﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Contains the methods that check if a given line is empty
    /// </summary>
    class EmptyLineHandler: AbstractHandler
    {
        /// <summary>
        /// Overrides the 'Handle' method, checks to see if the given input is empty
        /// </summary>
        /// <param name="request">The line to be checked</param>
        /// <param name="op">Instance of the 'Operation' class</param>
        /// <param name="var">Instance of the 'Variable' class</param>
        /// <param name="p">Instance of the 'Parser' class</param>
        /// <param name="method">Instance of the 'Method' class</param>
        /// <param name="args">The arguments of the command</param>
        /// <param name="run">Whether or not the program is in syntax-checking mode</param>
        /// <param name="fracturedLine">The raw line input</param>
        /// <param name="mainForm">Instance of the 'MainForm' form</param>
        /// <param name="fracturedLineValues">The values of any variables contained in the input</param>
        /// <returns>Either returns an informative string, or the base handler method - used to 'handle' the next object</returns>
        public override object Handle(object request, Operation op, Variable var, Parser p, Methods method, string[] args, bool run, string[] fracturedLine, MainForm mainForm, string[] fracturedLineValues)
        {
            if ((request as string).Equals("") == true) //If the request is empty, return an informative string, otherwise, call the 'Handle' method from the 'AbstractHandler' class - moves on to the next handler object
            {
                return "EmptyLine Recognised";
            }
            else
            {
                return base.Handle(request, op, var, p, method, args, run, fracturedLine, mainForm, fracturedLineValues);
            }
        }
    }
}

