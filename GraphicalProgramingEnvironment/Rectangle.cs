﻿using System.Drawing;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Handles manipulating and drawing a rectangle, inherits properties from the 'Shape' class
    /// </summary>
    class Rectangle : Shape
    {
        int width, height; //Creates the 'width' and 'height' variables for use in the class

        /// <summary>
        /// Handles setting up a 'Rectangle' with a default 'width' and 'height' of 20, also is passed the 'testingMode' boolean
        /// </summary>
        /// <param name="testingMode">Whether or not testing mode is active</param>
        public Rectangle(bool testingMode) : base(testingMode)
        {
            //Assiging 20 to both the 'width' and 'height' variables
            width = 20;
            height = 20;
        }
        /// <summary>
        /// Sets up a Rectangle with the various properties needed to draw it, along with the testingMode boolean for testing purposes
        /// </summary>
        /// <param name="pen">The pen that will be used to draw the rectangle</param>
        /// <param name="x">The 'x' value of the rectangle</param>
        /// <param name="y">The 'y' value of the rectangle</param>
        /// <param name="width">The width of the rectangle</param>
        /// <param name="height">The height of the rectangle</param>
        /// <param name="testingMode">Whether or not testing mode is active</param>
        public Rectangle(Pen pen, int x, int y, int width, int height, bool testingMode) : base(pen, x, y, testingMode)
        {
            //Setting the width and the height to that passed in this constructor
            this.width = width; 
            this.height = height;
        }
        /// <summary>
        /// Overrides the 'Set' method from 'Shape' to add the ability to set the width and height of the rectangle
        /// </summary>
        /// <param name="pen">The pen that will be used to draw the rectangle</param>
        /// <param name="list">The variable holding the various integers such as the width, height and the x and y positions</param>
        public override void set(Pen pen, params int[] list)
        {
            base.set(pen, list[0], list[1]); //Using the 'set' method from shape to set the pen, x and y values
            this.width = list[2]; //setting the width to the second (technically third) value in list[]
            this.height = list[3]; //setting the height to the third (technically fourth) value in list[]
        }
        /// <summary>
        /// Overrides the 'draw' method to add functionality to it
        /// Calls the 'DrawRectangle' method to graphically draw a rectangle
        /// </summary>
        /// <param name="g">Graphics Context</param>
        public override void draw(Graphics g)
        {
            g.DrawRectangle(pen, (x - (width / 2)), (y - (height / 2)), width, height); //Draws the rectangle, centered around the x and y position
        }
        /// <summary>
        /// Overrides the 'drawFilled' method to add functionality to it
        /// Calls the 'FillRectangle' method to graphically draw a filled rectangle
        /// </summary>
        /// <param name="g">Graphics Context</param>
        public override void drawFilled(Graphics g)
        { 
            SolidBrush solidB = new SolidBrush(pen.Color); //Creates a solid brush (used instead of normal pen to draw filled shapes)
            g.FillRectangle(solidB, (x - (width / 2)), (y - (height / 2)), width, height); //Draws the filled rectangle, centered around the x and y position
        }
    }
}
