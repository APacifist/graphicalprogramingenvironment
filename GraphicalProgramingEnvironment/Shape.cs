﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Creates a subclass Shape, inheriting the properties of 'Shapes'
    /// </summary>
  public abstract class Shape: IShapes
    {
        //Declaring attributes of the class (all shapes will invovle a pen and a location defined by x and y)
        /// <summary>
        /// An instance of a 'Pen' object - used for graphical operations
        /// </summary>
        protected Pen pen; //Defining 'pen' as a protected attribute
        /// <summary>
        /// Holds the 'x' and 'y' positions of the Pen inside the graphics environment
        /// </summary>
        protected int x, y;//Defining 'x' and 'y' as protected attributes
       /// <summary>
       /// This constructor allows for testingMode to be passed, which, if true, will skip any graphics-based declarations 
       /// that would otherwise produce errors during testing
       /// </summary>
       /// <param name="testingMode">Whether or not testing mode is active</param>
        public Shape(bool testingMode)
        {
            if(!testingMode)//Unless testingMode is true, set up the Pen colour and width variables
            {
                this.pen = new Pen(Color.Black,5);
                
            }
            x = y = 0; //This is not a graphics-based command, and so it can run regardless 
        }
        /// <summary>
        /// Sets up shape with the various properties needed to draw a shape, along with the testingMode boolean for testing purposes
        /// </summary>
        /// <param name="pen">The pen that will be used to draw the shape</param>
        /// <param name="x">The 'x' value of the shape's location</param>
        /// <param name="y">The 'y' value of the shape's location</param>
        /// <param name="testingMode">Whether or not testing mode is active</param>
        public Shape(Pen pen, int x, int y,bool testingMode)
        {
            if (!testingMode) //Unless testingMode is true, set the class' pen variable to be equal to that of the parameter being passed here
            {
                this.pen = pen;
                
            }
            //Again, these two can run regardless as they're nothing to do with graphics
            this.x = x; 
            this.y = y;
        }
        /// <summary>
        /// A placeholder method for drawing a shape, to be properly defined in subsequent subclasses
        /// </summary>
        /// <param name="g">The graphics context</param>
        public abstract void draw(Graphics g); //Don't need to define this yet
        /// <summary>
        /// A placeholder method for drawing a filled shape, to be properly defined in subsequent subclasses
        /// </summary>
        /// <param name="g"></param>
        public abstract void drawFilled(Graphics g);//Don't need to define this yet either
        /// <summary>
        /// A method for setting a shape's pen, as well as it's 'x' and 'y' position
        /// </summary>
        /// <param name="pen">The pen to be used for the current shape</param>
        /// <param name="list">The 'x' and 'y' integers that define the shape's position</param>
        public virtual void set(Pen pen,params int[] list)//Allows a shape - and any subclass of it, to have its pen set
        {

            this.pen = pen; //set the class' pen variable to be equal to that of the parameter being passed here 
            this.x = list[0]; //set the class' 'x' variable to be equal to the first integer of the list parameter
            this.y = list[1]; //set the class' 'x' variable to be equal to the second integer of the list parameter

        }
        
    }
}
