﻿using System.Text.RegularExpressions;
using System;
namespace GraphicalProgramingEnvironment
{

    /// <summary>
    /// Implements the 'ICommands' interface, and gives more detailed implementations of the interfaces 'execute' method
    /// </summary>
    public abstract class Command : ICommands
    {
        /// <summary>
        /// A method to set data within a class, declaring the method as abstract allows it to be defined later in subclasses
        /// </summary>
        /// <param name="run">Whether or not the method is being run in a test environment</param>
        /// <param name="list">The arguments to be passed to the method</param>
        public abstract void Set(bool run = true, params string[] list);

        /// <summary>
        /// A method to retrieve data within a class, declaring the method as abstract allows it to be defined later in subclasses
        /// </summary>
        /// <param name="args">The arguments to be passed to the method</param>
        /// <returns></returns>
        public abstract string Get(params string[] args);

        /// <summary>
        /// A method to find a string value within the inputed lines, can start from the beginning, or a given line number
        /// </summary>
        /// <param name="main">The main form to retrive the inputted lines from</param>
        /// <param name="searchTarget">The string to be searched for</param>
        /// <param name="count">The line number, set to 0 by default</param>
        /// <returns></returns>
        public virtual int FindStr(MainForm main, string searchTarget, int count = 0) //Searches for a given string from a given line
        {
            string[] textLines = main.activeWindow.Lines; //Takes each line of the given textbox and stores it in the 'cmdLines' array
            Console.WriteLine("LOOKHERE: " + count);
            // Loop through the array, format, and send the formatted contents to the 'ParseCommand' method
            for (int i = count; i < textLines.Length; i++) //For loop starts at 'count' which is either set by the user, or defaults to 0
            {
                if(textLines[i].ToLower().Equals(searchTarget))
                {
                    Console.WriteLine("String: \"" + searchTarget + "\" found! (Line: " + i + ")"); //Debugging purposes only
                    return i; //If the value is found, return the current line number
                }
            }
            Console.WriteLine("No ENDIF Found...");
            return -1; //Otherwise return -1 
        }

    }
}

