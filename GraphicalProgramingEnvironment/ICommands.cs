﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Contains basic descriptions of methods for commands that will be utilised, and implemented in other classes
    /// </summary>
    interface ICommands
    {
        /// <summary>
        /// The skeletal structure for the 'execute' method, used to execute commands
        /// </summary>
        /// <param name="run">Whether or not the method should run in syntax-checking mode</param>
        /// <param name="list">Parameters to be passed to the command</param>
        void Set(bool run = true, params string[] list); //Lays the foundations for the 'execute' method
                                        

    }
}
