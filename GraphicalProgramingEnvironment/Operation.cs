﻿using System;
using System.Text.RegularExpressions;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Provides methods to check the format and arguments of commands, and perform mathmatical operations
    /// - By Joshua Hooper: 33532281
    /// </summary>
    public class Operation
    {
        //Setting up the regex strings
        /// <summary>
        /// Regex for a maths operation
        /// </summary>
        public string genericOp = @"\([A-Za-z]+\s?[-+<>/*]?(==)?\s?[A-Za-z]+\)";
        /// <summary>
        /// Regex for maths operators: =  -  +  etc.
        /// </summary>
        public string mathsOpSymbol = @"[-+<>/*=]{1}|(==){1}";
        /// <summary>
        /// Regex for an 'if' statement
        /// </summary>
        public string ifStatement = @"if\s?\(\s?[A-Za-z0-9]+\s?([-+<>/*=]?(==)?\s?[A-Za-z0-9]+\s?)+\)";
        /// <summary>
        /// Regex for a variable declaration
        /// </summary>
        public string declaration = @"\s?[\w]+\s?([-+<>/*=]|(==))\s?([\w]+|[\d]+)\s?";
        /// <summary>
        /// Regex for a while loop
        /// </summary>
        public string whileLoop = @"while\s?\(\s?[A-Za-z0-9]+\s?\S(.)?(==)?\s?[A-Za-z0-9]+\s?\)";
        /// <summary>
        /// Regex for a method being called
        /// </summary>
        public string methodCall = @"[\w]+\(\)";
        /// <summary>
        /// Regex for a method being declared
        /// </summary>
        public string methodDeclaration = @"def\s[\w]+\(\)";
        /// <summary>
        /// Regex for an operation, with a variable assignment also
        /// </summary>
        public string opWithAssignment = @"[a-zA-Z]+\s?=\s?[a-zA-Z0-9]+\s?[-+/*]\s?[a-zA-Z0-9]+";
        /// <summary>
        /// Stored instance of the Variable class, allows variables declared by the user in other areas of the program, to be accessed here
        /// </summary>
        public Variable var; //Defining it here means the rest of the methods in this class can access it

        /// <summary>
        /// Default constructor to set up the 'var' variable 
        /// </summary>
        /// <param name="var">Passes an instance of the 'var' variable so that user-stored variables are persistant</param>
        public Operation(Variable var)
        {
            this.var = var; //Assigns 'var' to the variable defined above, allows it to be accessible throughout the class
        }
        /// <summary>
        /// Attempts to find a regex match for a given string
        /// </summary>
        /// <param name="line">The line to be checked for a match</param>
        /// <returns>The relevant string for the match, or 'None_Found' if no match is found</returns>
        public string Matches(string line)
        {
            if (Regex.IsMatch(line, ifStatement)) //Each 'if' of this method checks if the given string matches its regex line
            {
                return "IF_Statement";//Returns the type of regex match 
            }
            else if (Regex.IsMatch(line, whileLoop))
            {
                return "While_Loop";//Returns the type of regex match 
            }
            else if (Regex.IsMatch(line, methodDeclaration))
            {
                return "Method_Declaration";//Returns the type of regex match 
            }
            else if (Regex.IsMatch(line, methodCall))
            {
                return "Method_Call";//Returns the type of regex match 
            }
            else if (Regex.IsMatch(line, opWithAssignment))
            {
                return "Op_With_Assignment";//Returns the type of regex match 
            }
            else if (Regex.IsMatch(line, declaration))
            {
                return "Declaration";//Returns the type of regex match 
            }
            else if (Regex.IsMatch(line, genericOp))
            {
                return "Generic_Operation";//Returns the type of regex match 
            }
            else if (Regex.IsMatch(line, mathsOpSymbol))
            {
                return "Valid_Maths_Operator";//Returns the type of regex match 
            }
            else
            {
                Console.WriteLine("No matches...");//Debugging purposes only
                return "None_Found";//Returns the type of regex match 
            }

        }
        /// <summary>
        /// Checks if the given string matches the regex for a mathmatical operator
        /// </summary>
        /// <param name="line">The string to be validated</param>
        /// <returns></returns>
        public bool CheckOp(string line)
        {
            if (Regex.IsMatch(line, mathsOpSymbol)) //If the string matches the regex return true, otherwise return false
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Performs an operation, using a given mathmatical operator to determine which one to perform
        /// </summary>
        /// <param name="run">Whether or not the method has been called in syntax-checking mode</param>
        /// <param name="args">The values and operation to perform</param>
        /// <returns></returns>
        public string PerformOp(bool run = true, params string[] args)
        {
            try
            {
                switch (args[0]) //args[0] is the symbol for the operation to perform
                {
                    case "+":
                        Console.WriteLine("Operation: +");//Debugging purposes only
                        return (int.Parse(args[1]) + int.Parse(args[2])).ToString(); /*Parses the arguments as integers, performs the given
                                                                                  operation, then returns the result as a string*/
                    case "-":
                        Console.WriteLine("Operation: -");//Debugging purposes only
                        return (int.Parse(args[1]) - int.Parse(args[2])).ToString();/*Parses the arguments as integers, performs the given
                                                                                  operation, then returns the result as a string*/
                    case "*":
                        Console.WriteLine("Operation: *");//Debugging purposes only
                        return (int.Parse(args[1]) * int.Parse(args[2])).ToString();/*Parses the arguments as integers, performs the given
                                                                                  operation, then returns the result as a string*/
                    case "/":
                        Console.WriteLine("Operation: /");//Debugging purposes only
                        return (int.Parse(args[1]) / int.Parse(args[2])).ToString();/*Parses the arguments as integers, performs the given
                                                                                  operation, then returns the result as a string*/
                    case "<":
                        Console.WriteLine("Operation: <");
                        return LessThan(int.Parse(args[1]), int.Parse(args[2])).ToString();/*Parses the arguments as integers, calls the appropriate
                                                                                        * method, passing the integer values to it, and 
                                                                                        * returns the result (true/false) from the method as 
                                                                                        * a string*/
                    case ">":
                        Console.WriteLine("Operation: >");
                        return GreaterThan(int.Parse(args[1]), int.Parse(args[2])).ToString();/*Parses the arguments as integers, calls the appropriate
                                                                                        * method, passing the integer values to it, and 
                                                                                        * returns the result (true/false) from the method as 
                                                                                        * a string*/
                    case "=":
                        Console.WriteLine("Operation: =");
                        Console.WriteLine("Assigning args[1]: " + args[1] + " and args[2]: " + args[2]);
                        var.Set(run, args[1], args[2]);//Calls the variable assignment method, passing the given values along to be assigned
                        return "assigned";
                    case "==":
                        Console.WriteLine("Operation: ==");
                        Console.WriteLine("Comparing - args[1]: " + args[1] + " and args[2]: " + args[2]);
                        return EqualTo(int.Parse(args[1]), int.Parse(args[2])).ToString();/*Parses the arguments as integers, calls the appropriate
                                                                                        * method, passing the integer values to it, and 
                                                                                        * returns the result (true/false) from the method as 
                                                                                        * a string*/
                    default:
                        throw new Exception("Invalid operator: \"" + args[0] + "\""); //If no symbol is found, an error is thrown to inform the user

                }
            }
            catch
            {
                throw new Exception("Invalid operation: \"" + args[1] + " " + args[0] + " " + args[2] + "\""); //If an int.parse fails, an exception is thrown to inform the user
            }
        }
        /// <summary>
        /// Finds out if one value is less than another
        /// </summary>
        /// <param name="arg1">Value one</param>
        /// <param name="arg2">Value two</param>
        /// <returns>If value one is less than value two, return true, otherwise return false</returns>
        public bool LessThan(int arg1, int arg2)
        {

            if (arg1 < arg2) //If value one is less than value two, return true, otherwise return false
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Finds out if one value is greater than another
        /// </summary>
        /// <param name="arg1">Value one</param>
        /// <param name="arg2">Value two</param>
        /// <returns>If value one is greater than value two, return true, otherwise return false</returns>
        public bool GreaterThan(int arg1, int arg2)
        {
            if (arg1 > arg2)//If value one is greater than value two, return true, otherwise return false
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Finds out if one value is equal to another
        /// </summary>
        /// <param name="arg1">Value one</param>
        /// <param name="arg2">Value two</param>
        /// <returns>If value one is equal to value two, return true, otherwise return false</returns>
        public bool EqualTo(int arg1, int arg2)
        {
            if (arg1 == arg2)//If value one is equal to value two, return true, otherwise return false
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Adds two values together
        /// </summary>
        /// <param name="arg1">Value one</param>
        /// <param name="arg2">Value two</param>
        /// <returns>Returns the result of adding two values</returns>
        public int Add(int arg1, int arg2)
        {
            return (arg1 + arg2); //Returns the result of adding value one and value two
        }
        /// <summary>
        /// Subtracts value two from value one 
        /// </summary>
        /// <param name="arg1">Value one</param>
        /// <param name="arg2">Value two</param>
        /// <returns>Returns the result of subtracting two values</returns>
        public int Sub(int arg1, int arg2)
        {
            return (arg1 - arg2);//Returns the result of subtracting value two from value one
        }
        /// <summary>
        /// Multiplies two values 
        /// </summary>
        /// <param name="arg1">Value one</param>
        /// <param name="arg2">Value two</param>
        /// <returns>Returns the result of multiplying two values</returns>
        public int Mult(int arg1, int arg2)
        {
            return (arg1 * arg2); //Returns the result of multiplying value one and value two

        }
        /// <summary>
        /// Divides two values 
        /// </summary>
        /// <param name="arg1">Value one</param>
        /// <param name="arg2">Value two</param>
        /// <returns>Returns the result of dividing two values</returns>
        public int Div(int arg1, int arg2)
        {
            return (arg1 / arg2);//Returns the result of dividing value one by value two
        }
        /// <summary>
        /// Performs a multi-stage operation e.g. A = 1 + 2
        /// </summary>
        /// <param name="varName">The variable to have the result of the operation assigned to</param>
        /// <param name="run">Whether or not the program is in syntax-checking mode</param>
        /// <param name="args">The arguments of the operation</param>
        public void MultStageOp(string varName, bool run = true, params string[] args)
        {
            if (!(Matches(args[1]).Equals("Valid_Maths_Operator"))) //Throw an error if the second argument is not a valid mathmatical operator
            {
                throw new Exception("ERROR: Invalid Operator: \"" + args[1] + "\"");
            }
            else //Commence the operation otherwise
            {

                string retVal; //Variable to hold the initial result of the operation's first step
                int[] opLocs = new int[2]; //Holds the index locations of mathmatical operators
                bool flag = false; //Used to run a diffent operation depending on how many times a loop has run
                Console.WriteLine("Split into: [" + args[0] + "]  [" + args[1] + "]  [" + args[2] + "]  [" + args[3] + "]  [" + args[4] + "]");//Debugging purposes only
                for (int i = 0; i < args.Length; i++)
                {
                    if (CheckOp(args[i])) //Searches for a mathmatical operator, will be two, stores the first in 'opLocs' slot one, the other in slot two
                    {                     //The regex match ensures that there are two, if there weren't, an exception would have thrown
                        if (flag)
                        {
                            opLocs[1] = i;
                        }
                        else
                        {
                            opLocs[0] = i;
                            flag = true;
                        }

                    }
                }
                Console.WriteLine(opLocs[0]);//Debugging purposes only
                Console.WriteLine(opLocs[1]);//Debugging purposes only
                //Using the location of the operators to determine where the variables/value are
                //Variables/values should be either side of the maths operators, which is what the declarations below use to assign themselves
                int temp1 = opLocs[1]; 
                int temp2 = opLocs[1] + 1;
                int temp3 = opLocs[1] - 1;

                retVal = PerformOp(run, args[temp1], args[temp2], args[temp3]); //Performs the far-right operation first, storing the result in 'retVal'
                Console.WriteLine("[!] RETVAL: " + retVal);//Debugging purposes only
                temp1 = opLocs[0]; //For the last operation, only one assignment is needed
                Console.WriteLine("Operation: " + args[temp1] + " Variable: " + varName + " Value: " + retVal);//Debugging purposes only
                PerformOp(run, args[temp1], varName, retVal);//Takes the initially passed 'varName' variable, and assigns the value of 'retVal' to it
                //Uses the 'PerformOp' method to, in turn, call the variable assignment ('Set') method from the 'Variable' class
            }
        }
    }

}
