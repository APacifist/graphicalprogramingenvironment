﻿using System;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Responsible for storing, and keeping track of user-defined variables as well as their values, also controls the getting and setting of
    /// these variables
    /// - By Joshua Hooper: 33532281
    /// </summary>
    public class Variable : Command
    {
        /// <summary>
        /// The array in which the user-defined variables have their names, and coinciding values, stored
        /// </summary>
        public string[,] varArray = new string[20,2];
        int lastAddedX;
        int lastAddedY;
        int foundAt;
        /// <summary>
        /// Handles the setting up of default values upon creation of a 'Variable' object
        /// </summary>
        public Variable()
        {
           // varArray = new string[20, 2];
            lastAddedX = 0;
            lastAddedY = 0;

        }
         //[!] MIGHT NEED TO DELETE THIS (ITS IN THE COMMANDS CLASS TOO)
      /*  public override void ExecuteLoop(MainForm main, Variable var, Parser p, int start, bool run)
        {
            throw new NotImplementedException();
        }*/
        /// <summary>
        /// Controls the retrieving of user-defined variables. Gives detailled debugging info.
        /// User-defined variables are stored in a 2d Array, each section contains a variable's name and value
        /// Searches the variable array for duplicate values before storing new ones.
        /// </summary>
        /// <param name="args">The variable to look for</param>
        /// <returns>Returns the stored value of the requested variables</returns>
        public override string Get(params string[] args)
        {
            if(varArray.Length != 0) { //Skip the whole search if the array is empty
                for (int k = 0; k < 20; k++) //Loop through the array looking at the first column(the variable name column)
                {
                    try
                    {
                        if (varArray[k, 0] != null) //Don't bother trying to read it if it's null
                        {
                            Console.WriteLine("Analysing: " + varArray[k,0] + ", does it look like " + args[0] + "?");//Debugging purposes only
                            if (varArray[k, 0].Equals(args[0])) //
                            {
                                Console.WriteLine("Found it!");
                                foundAt = k;
                                return varArray[k, 1];
                            }
                            Console.WriteLine("Nope...");
                        }
                    }
                    //Just in case the method attempts to compare a null value, this catch should avoid the program crashing
                    catch (NullReferenceException)
                    {
                        Console.WriteLine("Null reference reached inside 'Get' of 'Variable' class");
                    }
                }
             }
            Console.WriteLine("I Couldn't find it...");
            return "false"; //If the loop finishes without breaking out at any point, the value isn't present, so 'false' is returned 
        }

        /// <summary>
        /// Overrides the set method to allow new values to be added to the variable array
        /// </summary>
        /// <param name="list">Contains the name and value of the variable being stored</param>
        /// <param name="run">Whether the method should run in syntax-checking mode</param>
        public override void Set(bool run = true, params string[] list)
        {
            int tempX = 0; //Setting default x and y references for the 2d array
            int tempY = 0;

            /*setting the above variables to be equal to the location values of the last added variable so the next
             * added variable is placed into a new slot
             */
             tempX += lastAddedX;  
             tempY += lastAddedY;   
            if (this.Get(list[0]).Equals("false")) //First, check that an entry of the variable doesn't already exist
            {//If its a new one, go through the process of adding it.
                Console.WriteLine();
                Console.WriteLine("---Params---\n" + list[0] + "," + list[1] + "\n---Array Target---");
                Console.WriteLine(lastAddedX + ","+ lastAddedY + "\n------------------");
                varArray[tempX, tempY] = list[0];
                varArray[tempX, (tempY + 1)] = list[1];
                Console.WriteLine("New values of array position: " + tempX + "," + tempY + " & " + tempX + "," + (tempY+1) + " = " + varArray[tempX, tempY] + ", " + varArray[tempX, (tempY+1)] + "\n");
                if (lastAddedX < 20) //Since there are limited slots, if the last added variable is at that limit, it swings back around to zero
                {
                    lastAddedX += 1;
                }
                else
                {
                    lastAddedX = 0;
                }
            }
            else //If an entry for the variable already exists, override it with the new value
            {
                varArray[foundAt, tempY] = list[0];
                varArray[foundAt, (tempY + 1)] = list[1];
            }
        }
    }
}
