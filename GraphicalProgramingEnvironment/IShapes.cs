﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Contains basic methods for Shapes that will be utilised in other classes
    /// </summary>
    interface IShapes
    {
        /// <summary>
        /// Lays the foundations for the 'set' method of anything that implements this interface
        /// </summary>
        /// <param name="pen">The pen to be used during the operation</param>
        /// <param name="list">The integer values to be used within the operation</param>
        void set(Pen pen, params int[] list); //Lays the foundations for the 'set' method
        /// <summary>
        /// Defines a skeletal 'draw' method to be properly defined in an implementing class
        /// </summary>
        /// <param name="g">Graphics context</param>
        void draw(Graphics g); //Lays the foundations for the 'draw' method
        /// <summary>
        /// Defines a skeletal 'drawFilled' method to be properly defined in an implementing class
        /// </summary>
        /// <param name="g"></param>
        void drawFilled(Graphics g); //Lays the foundations for the 'drawFilled' method
    }
}
