﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Holds the method responsible for calling handler objects
    /// </summary>
    class Client
    {
        /// <summary>
        /// Calls the given handler object
        /// </summary>
        /// <param name="handler">The handler object</param>
        /// <param name="line">The inputted line</param>
        /// <param name="op">Instance of the 'Operation' class</param>
        /// <param name="var">Instance of the 'Variable' class</param>
        /// <param name="p">Instance of the 'Parser' class</param>
        /// <param name="method">Instance of the 'Method' class</param>
        /// <param name="args">The arguments of the command</param>
        /// <param name="run">Whether or not the program is in syntax-checking mode</param>
        /// <param name="fracturedLine">The raw line input</param>
        /// <param name="mainForm">Instance of the 'MainForm' form</param>
        /// <param name="fracturedLineValues">The values of any variables contained in the input</param>
        /// <returns>Returns 'true' if a handler class recognises the input, or 'false' if not</returns>
        public static Boolean ClientCode(AbstractHandler handler, string line, Operation op, Variable var, Parser p, Methods method, string[] args, bool run, string[] fracturedLine, MainForm mainForm, string[] fracturedLineValues)
        {
            //Passes the neccesary arguments to the passed handler object.
                var result = handler.Handle(line, op, var, p, method, args, run, fracturedLine, mainForm, fracturedLineValues);

                if (result != null) /*If the 'result' variable is null, then the handler didn't recognise the string, so return 'false'
                                     * if it has a value then it did recognise the given string, so return 'true'*/
                {
                    Console.Write("Recognised");//Debugging purposes only
                return true;
            }
                else
                {
                return false;
                }          
        }
    }
}
