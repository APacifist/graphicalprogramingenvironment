﻿using System;

namespace GraphicalProgramingEnvironment
{
    /// <summary>
    /// Contains the methods to facilliate the useage of IF statements
    /// - By Joshua Hooper: 33532281
    /// </summary>
    class IfStatement : Command
    {
        string variableValue_1;
        string variableValue_2;
        string operandValue;
        string variableValue_1_Name;
        string variableValue_2_Name;

        public override string Get(params string[] args)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Allows the setting of this class' variables, such as names of variables and operations to perform on them
        /// </summary>
        /// <param name="list"> The values to be set</param>
        /// <param name="run">Whether the method is being called in syntax-checking mode</param>
        public override void Set(bool run = true, params string[] list)
        {
            variableValue_1_Name = list[0];
            variableValue_2_Name = list[2];
            operandValue = list[1];
        }
        /// <summary>
        /// Performs the operations of an IF statement 
        /// </summary>
        /// <param name="main">The main form</param>
        /// <param name="var">Referencing an object of the 'Variable' class</param>
        /// <param name="p">Referencing an object of the 'Parser' class</param>
        /// /// <param name="run">Whether the method is in syntax-checking mode</param>
        /// /// <param name="count">The number of the current line being executed on</param>
        /// /// <param name="isLooping">Whether this method is being called whilst inside a loop</param>
        public void ExecuteIf(MainForm main, Variable var, Parser p, bool run = true, int count = 0, bool isLooping = false)
        {
            int endLine;
            Console.WriteLine("IF: Starting variable assignments");
            /*Checks if the values being operated on are variables, if they are, they store their values in one of the 'variableValue' variables, 
            if not, then they must be integers and so they are directly stored in the relevant 'variableValue' variable*/
            if (!isLooping)
            {
                endLine = FindStr(main, "endif", p.counter);  //Attempts to find the string 'endif' inside the textbox's text
                Console.WriteLine("BLAH: NOT-Loopmode if statement");
            }
            else
            {  
            Console.WriteLine("BLAH: Loopmode if statement");
                endLine = FindStr(main, "endif", count);  //Attempts to find the string 'endif' inside the textbox's text
            }
               
            Console.WriteLine($"IF_STATEMENT searching from 'count': {count}"); //Count passed by loop
            if (endLine != -1) //As long as 'FindStr' doesn't store '-1' in 'endLine', run the rest of the code 
            {
                if (var.Get(variableValue_1_Name).Equals("false"))
                {
                    variableValue_1 = variableValue_1_Name;
                }
                else
                {
                    variableValue_1 = var.Get(variableValue_1_Name);
                }
                if (var.Get(variableValue_2_Name).Equals("false"))
                {
                    variableValue_2 = variableValue_2_Name;
                }
                else
                {
                    variableValue_2 = var.Get(variableValue_2_Name);
                }
                Console.WriteLine("IF: Performing Operation");
                Operation op = new Operation(var); //Instantiates a new object of the 'Operation' class
                Console.WriteLine("IF: Finished Operation");
                bool operation = bool.Parse(op.PerformOp(run, operandValue, variableValue_1, variableValue_2));
                /*(Above line) Calls the 'PerformOp' method of the 'Operation' class, passing two values and a desired operation, storing
                 * the result in the 'operation' variable*/
                Console.WriteLine("IF: Operation -" + operation);
                if (!operation)
                //If the 'PerforOp' method returns 'false', then the IF contition has not been met, so the code inside the IF statement is skipped
                {
                    p.skipLines = true;
                    p.skipTo = endLine;
                    Console.WriteLine("IF Failure, jumping to line: " + (endLine + 1) + " (Index: " + endLine + ")" + " Count is : " + p.counter);
                }

            }
            else //If the 'FindStr' method returns '-1' then the string wasn't found, so throw an exception
            {
                throw new Exception("ERROR: No enclosing 'endif' found");
            }
        }

    }
}
