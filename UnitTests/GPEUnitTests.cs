using GraphicalProgramingEnvironment;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTests
{
    /// <summary>
    /// Contains the unit test methods for the Graphical Programing Environment project
    /// - by Joshua Hooper: 33532281
    /// </summary>
    [TestClass]
    public class GPEUnitTests
    {


        //Part 1 Unit Tests//

        /// <summary>
        /// Instantiates the parser class, with  the 'testing' boolean as an argument which should prevent any graphical code running.
        /// It then calls 'ParseCommand' method and passes a string containing an invalid command in order to test the application's response to such an input.
        ///  /// For the test to succeed, the program should produce an exception detailing the passing of an invalid command.
        /// </summary>
        /// 
        [TestMethod]
        public void TestParseInvalidCommand()
        {
            bool testing = true;
            string error = "";
            Parser p = new Parser(testing);
            try
            {
                p.ParseCommand("invalidcommand ran dom"); //Passes the invalid command string
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            Assert.AreEqual("Unrecognised command", error);

        }
        /// <summary>
        /// Instantiates the parser class, with  the 'testing' boolean as an argument which should prevent any graphical code running.
        /// It also references an instance of the Canvas class instantiated inside the Parser class.
        /// It then calls the 'ParseCommand' method and passes a string containing a valid command string to test the application's response.
        ///  /// For the test to succeed, the application should update the xPos and yPos variables to the new positions specified in the command string provided.
        /// </summary>
        [TestMethod]
        public void ParseValidMoveToInput()
        {
            bool testing = true;
            //  Canvas c = new Canvas(true);
            Parser p = new Parser(testing);
            Canvas c = p.myCanvas; //Creating a canvas object this way avoids dealing with this project not containing any graphics functionality
            p.ParseCommand("moveto 100 100"); //Passes the valid command string for the moveTo command

            Assert.AreEqual(100, c.xPos, 0.01);
            Assert.AreEqual(100, c.yPos, 0.01);
        }
        /// <summary>
        /// Instantiates the parser class, with  the 'testing' boolean as an argument which should prevent any graphical code running.
        /// It then calls the 'ParseCommand' method and passes a partially valid command string to test the application's response.
        /// The test string has the correct command, but an incorrect number of arguments.
        /// For the test to succeed, the application should report the issue in an exception.
        /// </summary>
        [TestMethod]
        public void ParseInvalidMoveToInput()
        {
            bool testing = true;
            string error = "";
            Parser p = new Parser(testing);
            try
            {
                p.ParseCommand("moveto 100"); //Passes the valid command string for the moveTo command with an invalid number of arguments
            }
            catch (ArgumentException e)
            {
                error = e.Message;
            }
            Assert.AreEqual("ERROR: Invalid number of arguments", error);

        }
        /// <summary>
        /// Instantiates the parser class, with  the 'testing' boolean as an argument which should prevent any graphical code running.
        /// It then calls the 'ParseCommand' method and passes a  valid command string containing boundary data to test the application's response.
        /// The test string has the correct command, and valid parameters, but ones that would result in going outside of the boundary of the bitmap.
        /// For the test to succeed, the application should report the issue in an exception.
        /// </summary>
        [TestMethod]
        public void ParseBoundaryMoveToInput()
        {
            bool testing = true;
            string error = "";
            Parser p = new Parser(testing);
            try
            {
                p.ParseCommand("moveto 600,20"); //Passes the valid command string for the moveTo command, containing the boundary argument values
            }
            catch (FormatException e)
            {
                error = e.Message;
            }
            Assert.AreEqual("invalid parameters", error);

        }
        /// <summary>
        /// Instantiates the parser class, with  the 'testing' boolean as an argument which should prevent any graphical code running.
        /// It also references an instance of the Canvas class instantiated inside the Parser class.
        /// It then calls the 'ParseCommand' method and passes a valid command 'center' to test the application's response.
        /// For the test to succeed, the application should update the xPos and yPos variables in to those specified in the center command's code.
        /// </summary>
        [TestMethod]
        public void ParseValidCenterCmd()
        {
            bool testing = true;
            Parser p = new Parser(testing);
            Canvas c = p.myCanvas;
            p.ParseCommand("center"); //Passes the valid command string
            Assert.AreEqual(293, c.xPos, 0.01);
            Assert.AreEqual(217, c.yPos, 0.01);
        }


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////Part 2 Unit Tests////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
       
        ///
        /// <summary>
        /// Instantiates the factory class and attempts to pass an invalid shape's name to the 'getShape' method.
        /// The test succeeds if the method returns an exception explaining the issue.
        /// </summary>
        [TestMethod]
        public void CallInvalidFactoryGetShape()
        {
            bool testing = true;
            string error = "";
            Factory f = new Factory();
            try
            {
                f.GetShape("Rhombus", testing); //Passes the invalid shape name
            }
            catch (ArgumentException e)
            {
                error = e.Message;
            }
            Assert.AreEqual("Factory error: rhombus does not exist", error);
        }
        /// <summary>
        /// Instantiates the factory class and attempts to pass valid shape's name to the 'getShape' method.
        /// The test succeeds if the method returns an object of that shape without returning an error.
        /// </summary>
        [TestMethod]
        public void CallValidFactoryGetShape()
        {
            bool testing = true;
            Factory f = new Factory();
            Shape s = f.GetShape("Square", testing); //Passes the valid shape name
        }

        ///
        /// <summary>
        /// Instantiates the factory class and attempts to pass an valid command's name to the 'GetCommand' method.
        /// The test succeeds if the method returns an exception explaining the issue.
        /// </summary>
        [TestMethod]
        public void CallInvalidFactoryGetCommand()
        {
            string error = "";
            Factory f = new Factory();
            try
            {
                f.GetCommand("InvalidCommand"); //Passes the invalid shape name
            }
            catch (ArgumentException e)
            {
                error = e.Message;
            }
            Assert.AreEqual("Unrecognised command", error);
        }
        ///
        /// <summary>
        /// Instantiates the factory class and attempts to pass valid commands's name to the 'GetCommand' method.
        /// The test succeeds if the method returns an object of that command without returning an error.
        /// </summary>
        [TestMethod]
        public void CallValidFactoryGetCommand()
        {
            Factory f = new Factory();
            Command cmd = f.GetCommand("If_statement"); //Passes the valid command name
        }


        /// <summary>
        /// Tests that a variable can be created and have a value assigned to it, then ensures that the variable can then be found, and its value retrieved
        /// </summary>
        [TestMethod]
        public void StoreAndRetrieveValueInVariable()
        {
            Variable var = new Variable();//Instantiate a new Variable object 
            String[] args = {"testVar","2"}; //Assign a variable name, and value to a new array           
            var.Set(list: args);//Pass the array to the 'Set' method of Variable

            String returnedResult = var.Get("testVar"); //Call the 'Get' method of variable, which searches for, and retrieves the value of the given variable
            Assert.AreEqual("2",returnedResult);//The method should return a string containing the value of the given variable

        }
        /// <summary>
        /// Tests how the program reacts if the 'Get' command of the 'Variable' class is run with a non-existant variable name pased to it
        /// </summary>
        [TestMethod]
        public void ValidStoreInvalidRetriveForVariable()
        {
            Variable var = new Variable(); //Instantiate a new Variable object 
            String[] args = { "testVar", "2" };//Assign a variable name, and value to a new array    
            var.Set(list: args); //Pass the array to the 'Set' method of Variable

            String returnedResult = var.Get("incorrectName");//Call the 'Get' method of variable, which searches for, and retrieves the value of the given variable
            Assert.AreEqual("false", returnedResult);//Since the variable searched for doesn't exist, the method should return 'false'
        }

        /// <summary>
        /// Tests how the parser reacts to a completely invalid 'while' loop string
        /// </summary>
        [TestMethod]
        public void TestParseInvalidWhileArgsNum()
        {
            bool testing = true;
            string error = "";
            Parser p = new Parser(testing);
            try
            {
                p.ParseCommand("while a @b"); //Passes the invalid 'while' string
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            Assert.AreEqual("Invalid number of parameters: Ensure arguments are seperated with whitespace", error);

        }
        /// <summary>
        /// Tests how the parser reacts to a string with a valid 'while' format, but an invalid comparison operator
        /// </summary>
        [TestMethod]
        public void TestParseInvalidWhileOperator()
        {
            bool testing = true;
            string error = "";
            Parser p = new Parser(testing);
            try
            {
                p.ParseCommand("while(a @ 2)"); //Passes the string containing a while loop with a valid format, but an invalid comparison operator
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            Assert.AreEqual("Invalid operator: \"@\" used in while loop", error);

        }
        /// <summary>
        /// Tests how the parser reacts to a valid 'while' loop command string
        /// </summary>
        [TestMethod]
        public void TestParseValidWhile()
        {
            bool testing = true;
            Parser p = new Parser(testing);
                p.ParseCommand("while(a < 2)"); //Passes the string containing a valid while loop       
            //If an error is thrown, the test will fail, otherwise, it passes as the command was recognised as a valid 'While' loop
        }
        /// <summary>
        /// Tests how the parser reacts to a valid 'IF' statement command string
        /// </summary>
        [TestMethod]
        public void TestParseValidIf()
        {
            bool testing = true;
            Parser p = new Parser(testing);
            p.ParseCommand("if(4 == 4)"); //Passes the string containing a valid 'IF' statement      
            //If an error is thrown, the test will fail, otherwise, it passes as the command was recognised as a valid 'IF' statement
        }
        /// <summary>
        /// Tests how the parser reacts to a valid 'IF' statement command string
        /// </summary>
        [TestMethod]
        public void TestParseInvalidIfFormat()
        {
            bool testing = true;
            string error = "";
            Parser p = new Parser(testing);
            try
            {
                p.ParseCommand("if(a == )"); //Passes the string containing an 'IF' statement loop with an invalid format
            }
            catch (Exception e)
            {
                error = e.Message;
            }
            Assert.AreEqual("Invalid command format, use: if([val/var] [operation] [val/var])", error);
        }
        /// <summary>
        /// Tests that a method can be created and have values assigned to it, then ensures that the method can then be found, and its values retrieved
        /// </summary>
        [TestMethod]
        public void StoreAndRetrieveMethod()
        {
            Methods method = new Methods();//Instantiate a new Methods object 
            String[] args = { "MethodName", "0","2" }; //Assign a method name, start, and end value to a new array           
            method.Set(list: args);//Pass the array to the 'Set' method of Methods

            String returnedResult = method.Get("MethodName"); //Call the 'Get' method of Methods, which searches for, and retrieves the values attatched to the given method
            Assert.AreEqual("0 2", returnedResult);//The method should return a string containing the start, and end value of the given method

        }
        /// <summary>
        /// Tests how the program reacts if the 'Get' method of the 'Methods' class is run with a non-existant method name pased to it
        /// </summary>
        [TestMethod]
        public void ValidStoreInvalidRetriveForMethod()
        {
            Methods method = new Methods();//Instantiate a new Methods object 
            String[] args = { "MethodName", "0", "2" }; //Assign a method name, start, and end value to a new array           
            method.Set(list: args);//Pass the array to the 'Set' method of Methods  

            String returnedResult = method.Get("incorrectName");//Call the 'Get' method of Methods, which searches for, and retrieves the values attatched to the given method
            Assert.AreEqual("false", returnedResult);//Since the method name given doesn't exist, the method should return 'false'
        }
        /// <summary>
        /// Tests that a multi-stage operaion can be performed in which a mathmatical operation is performed, and the result is
        /// assigned to a variable that is also initialised within the operation
        /// </summary>
        [TestMethod]
        public void ValidMultStageOperationCall()
        {
            Variable var = new Variable();
            Operation op = new Operation(var);
            String[] args = { "a","=", "1", "+","2" }; //Assign the parameters needed for a multiple operation command to the 'args' array    
            op.MultStageOp("a", true, args); /*Execute the method from the 'operation' class to execute the multi-stage operation that will add 1+2 and assign
                                              the result to 'a' - a variable that previously didn't exist, so will also be defined during the operation*/
            String returnedResult = var.Get("a"); //Call the 'Get' method of variable, which searches for, and retrieves the value of the given variable
            Assert.AreEqual("3", returnedResult);//The method should return a string containing the value "3" if the test is to be considered a success
        }

    }
}
